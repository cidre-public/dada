import logging
import scipy.stats as stats
import os
import pandas


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def count_from_dict(sample_dict):
    """
    Count the number of times
    :param sample_dict: A dictionary with an id as the key, and a value
    :return:
    """

    combination_count = {}
    num_entries = 0

    for element in sample_dict:
        # combination = tuple(sample_dict[element])
        combination = tuple(sample_dict[element].split(","))
        if combination in combination_count:
            combination_count[combination] += 1
        else:
            combination_count[combination] = 1
        num_entries += 1

    return combination_count, num_entries


def get_counts(values_path):
    """
    Get a dictionary of counts for the values
    :param values_path: Full path to the value file (It must be a .classcount.csv file)
    # FUTURE: Give example in docstring (or, better, make a test example)
    :return: A dictionary with combination (tuple) key and count value
    """
    num_entries = 0
    val_dict = {}
    with open(values_path) as f:
        for line in f:
            line_arr = line.strip().split(",")
            count = float(line_arr[0])
            # value = tuple(line_arr[1:])
            value = tuple(int(num) for num in line_arr[1:])
            val_dict[value] = count
            # if value == test_key:
            #     log.debugv("Found combination {} with value {}".format(value, count))
            num_entries += count

    # log.debugv("Obtained dict: {}".format(val_dict))
    # log.info("Value for {} in {}: {}".format(test_key, values_path, val_dict[test_key]))
    return val_dict, num_entries


def partial_chi2(exp_val, obs_val):
    return ((obs_val - exp_val)**2)/exp_val


def chi2_homogeneity_test(list_dataset, df_index, classes=None):
    """
    Chi-squared test of homogeneity

    :param list_dataset: List of list of values
    :param df_index:
    :param classes:
    :return:
        - tuple of Chi-squared result and p-value
        - number of classes used in the test
        - observed values DataFrame
        - expected values DataFrame
    """
    # if classes:
    #     df = pandas.DataFrame([d1, d2], index=df_index, columns=classes)
    # else:
    #     df = pandas.DataFrame([d1, d2], index=df_index).fillna(0)
    if classes:
        df = pandas.DataFrame(list_dataset, index=df_index, columns=classes).fillna(0)
    else:
        df = pandas.DataFrame(list_dataset, index=df_index).fillna(0)

    # list_drop = []
    # for k in df.columns:
    #     # if ev_df.at[ev_df.index[0], k] == 0 and df.at[ev_df.index[0], k] == 0:
    #     if df.at[df.index[0], k] == 0:
    #         list_drop.append(k)
    #         log.debug("Eliminating column {}".format(k))

    # # ev_df = ev_df.drop(list_drop, axis=1)
    # df = df.drop(list_drop, axis=1)

    # Create empty expected values DataFrame from the original
    ev_df = pandas.DataFrame(index=df_index, columns=df.columns).fillna(0.0)
    log.debugv("Table (DataFrame): \n{}\n".format(df))

    sum_cols = df.sum(0)
    sum_rows = df.sum(1)
    total = df.to_numpy().sum()

    log.debug("Total of cells: {}".format(total))
    log.debug("Sum of columns: \n{}\n".format(sum_cols))
    log.debug("Sum of rows: \n{}\n".format(sum_rows))

    observed_values = []
    expected_values = []

    ev_less_than_five = False
    cells_low_frequency = 0
    ev_less_than_one = False
    num_cells_less_than_one = 0

    list_drop = []
    for index, row in df.iterrows():
        # print("At index {}".format(index))
        for c_index, value in row.items():
            # print("This is the value of {}: {}".format(c_index, value))
            expected_value = sum_cols[c_index] * sum_rows[index] / total
            if expected_value < 5:
                ev_less_than_five = True
                cells_low_frequency += 1
            if expected_value < 1:
                ev_less_than_one = True
                num_cells_less_than_one += 1
            # print("type: {}".format(type(expected_value)))
            log.debugv("Expected value for {} {}: {}".format(index, c_index, expected_value))
            if expected_value == 0:
                log.debugv("CAUTION: Expected value is zero for class {}".format(c_index))
                index1 = df.index[0]
                log.debugv("- Observed value in {}: {}".format(index1, df.at[index1, c_index]))
                index2 = df.index[1]
                log.debugv("- Observed value in {}: {}".format(index1, df.at[index2, c_index]))
                list_drop.append(c_index)
                continue

            observed_values.append(value)
            expected_values.append(expected_value)
            # Add value to DataFrame
            ev_df.at[index, c_index] = expected_value

    ev_df = ev_df.drop(list_drop, axis=1)
    df = df.drop(list_drop, axis=1)

    log.debug("Len of observed_values: {}".format(len(observed_values)))
    log.debug("Len of expected_values: {}".format(len(expected_values)))
    return stats.chisquare(observed_values, f_exp=expected_values), len(df.columns), df, ev_df

    # print('Chi-squared test for \'{}\': {} (p-value: {})'
    #       .format("Dataset 1", chi2, pvalue))


def chi2_goodness_of_fit_test(exp_dict, exp_total, obs_dict, obs_total, population=True):
    corr_exp_dict = {}
    if population:
        # log.debug("Expected values from population")
        for key in exp_dict:
            if exp_dict[key] != 0:
                log.debugv("Old value: {}".format(exp_dict[key]))
                new_val = (exp_dict[key] / exp_total) * obs_total
                corr_exp_dict[key] = new_val
                log.debugv("New value: {}".format(corr_exp_dict[key]))
    else:
        corr_exp_dict = exp_dict

    expected_values = []
    observed_values = []

    expected_catch_all = 0
    observed_catch_all = 0

    # Important: Even if the features are not initially in order, it doesn't matter for the test (a sum of values)
    # It only matters that each combination value has its corresponding match
    for key in corr_exp_dict:
        if corr_exp_dict[key] < 5:
            expected_catch_all += corr_exp_dict[key]
            if key in obs_dict:
                observed_catch_all += obs_dict[key]
        else:
            expected_values.append(corr_exp_dict[key])
            if key in obs_dict:
                observed_values.append(obs_dict[key])
            else:
                observed_values.append(0)

    # Add default class at the end
    expected_values.append(expected_catch_all)
    observed_values.append(observed_catch_all)

    log.debug("Num of expected values: {}".format(sum(expected_values)))
    log.debugv("Test expected values: " + str(expected_values))
    log.debug("Num of observed values: {}".format(sum(observed_values)))
    log.debugv("Test observed values: " + str(observed_values))

    num_categories = len(expected_values)
    log.debug("Number of categories in the expected values: {}".format(num_categories))
    return stats.chisquare(observed_values, f_exp=expected_values), num_categories


def wrapper_chi_squared(ev_path, list_ov_path, output_dir, population_name, list_dataset_name,
                        test_type, population=False):
    """
    Wrapper function for the Chi-Squared test

    :param ev_path: Full path of the expected values (typically a .classcount.csv file)
    values
    :param list_ov_path: Full path of the observed values (typically a .classcount.csv file)
    :param output_dir:
    :param population: Tell if the expected values come from a population, in order to calculate the proper expected
    :param test_type: Either 'gof' for goodness of fit or 'h' for homogeneity
    :return: (nothing)
    """

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Get the count of observed values
    log.debug("Getting expected values")
    ev_dict, ev_freq = get_counts(ev_path)
    log.debug("Num of total expected values: {}".format(ev_freq))

    log.debug("Getting observed values")

    for ov_path, o_dataset_name in zip(list_ov_path, list_dataset_name):
        ov_dict, ov_freq = get_counts(ov_path)
        name = ' '.join(ov_path.split('/')[-1].split('.')[:-1])
        log.debug("Evaluating {}".format(name))
        log.debug("Num of total observed values: {}".format(ov_freq))

        if test_type == 'gof':
            dataset_names = [population_name, o_dataset_name]
            # cs, pvalue = chi2_homogeneity_test(ev_dict, ov_dict, dataset_names)[:2]
        cs, pvalue = chi2_goodness_of_fit_test(ev_dict, ev_freq, ov_dict, ov_freq, population)

        # Get name from the file name
        log.info('Chi-squared test for \'{}\': {} (p-value: {})'.format(name, cs, pvalue))


