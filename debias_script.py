import argparse
import configparser
import datetime
import logging
import copy
import os
import random
import numpy as np

import utilities
import count_instances as ci

import time
now_str = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level, base_dataset, target_dataset, deltap):
    if not os.path.exists("logs_debias_search_plus"):
        os.makedirs("logs_debias_search_plus")
    fh = logging.FileHandler('logs_debias_search_plus/output_debias_search_plus_' + 'delta-' + str(deltap) + '_' + str(base_dataset) + '_'
                             + str(target_dataset) + "_" + now_str + '.log', mode='w')
    formatter = logging.Formatter('%(asctime)s [%(levelname)8s] %(message)s')
    fh.setFormatter(formatter)
    log.addHandler(fh)
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        fh.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        fh.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        fh.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        fh.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


class Dataset:
    def __init__(self, combination_hashes, class_size_ext=None):
        self.items = combination_hashes
        if class_size_ext:
            self.class_size = class_size_ext
        else:
            s = {}
            for k in combination_hashes:
                s[k] = len(combination_hashes[k])
            self.class_size = s

        self.total = 0
        self.index = list(self.class_size.keys())
        for k in self.class_size:
            self.total += self.class_size[k]

    def get(self, k):
        return self.class_size[k]

    def sum(self):
        return self.total

    def update(self, k, num):
        self.class_size[k] += num
        self.total += num

    def get_non_empty(self):
        list_k = []
        for k in self.class_size:
            if self.class_size[k] > 0:
                list_k.append(k)
        return list_k

    def remove_one_app(self, k):
        this_hash = random.choice(self.items[k])
        self.items[k].remove(this_hash)

        return this_hash, k

    def add_one_app(self, this_hash, k):
        self.items[k].append(this_hash)


def get_t_delta(G_k, size_G, T_k, size_T):
    """
    Get the class proportion difference of G and T
    :param G_k: # apps of class 'k' in G
    :param size_G: # apps in G (in all classes)
    :param T_k: # apps of class 'k' in T
    :param size_T: # apps in T (in all classes)
    :return:
    """
    return (G_k / size_G) - (T_k / size_T)


def class_arg_max_min(generated_dataset, target_dataset, list_k=None):
    """
    Get the  classes with the highest and lowest difference between the generated_dataset and the target_dataset
    :param generated_dataset:
    :param target_dataset:
    :return:
    """
    k_h = None
    k_l = None
    max_delta = np.NINF
    min_delta = np.PINF

    if not list_k:
        # log.debug("  - Normal listing of global max and min classes")
        # Both generated and target dataset have the same classes
        for index in generated_dataset.index:
            # log.debugv("Trying with class {}".format(index))
            delta_t = get_t_delta(generated_dataset.get(index), generated_dataset.sum(),
                                  target_dataset.get(index), target_dataset.sum())

            if delta_t >= max_delta:
                k_h = index
                max_delta = delta_t

            if delta_t <= min_delta:
                k_l = index
                min_delta = delta_t
    else:
        # log.debug("  - A list was added to the arguments")
        for index in list_k:
            delta_t = get_t_delta(generated_dataset.get(index), generated_dataset.sum(),
                                  target_dataset.get(index), target_dataset.sum())

            if delta_t >= max_delta:
                k_h = index
                max_delta = delta_t

            if delta_t <= min_delta:
                k_l = index
                max_delta = delta_t

    return k_h, k_l


def get_max_delta(generated_dataset_md, target_dataset_md):
    """
    Get the the absolute max 't delta'
    :param generated_dataset_md:
    :param target_dataset_md:
    :return:
    """
    max_delta = 0
    for k in generated_dataset_md.index:
        delta_k = abs(get_t_delta(generated_dataset_md.get(k), generated_dataset_md.sum(),
                                  target_dataset_md.get(k), target_dataset_md.sum()))
        if delta_k >= max_delta:
            max_delta = delta_k
    return max_delta


def copy_these_sizes(combo_dict):
    """
    Copy (hard copy) a 'combination hashes' dictionary
    :param combo_dict:
    :return:
    """
    gen = {}
    for k in combo_dict:
        gen[k] = int(combo_dict[k])
    return gen


def copy_these_counts(combo_dict):
    """
    Copy (hard copy) a 'combination hashes' dictionary
    :param combo_dict:
    :return:
    """
    gen = {}
    for k in combo_dict:
        gen[k] = list(combo_dict[k])
    return gen


def getme_deltas(generated_dataset, target_dataset):
    """
    Calculate delta and absolute delta of generated and target dataset
    :param generated_dataset: An object of class Dataset
    :param target_dataset: An object of class Dataset
    :return:
    """
    delta_G_T = {}
    abs_delta_G_T = {}
    # log.debugv("Calculating initial max deltas")
    # log.debugv('- Size G: {}'.format(generated_dataset.sum()))
    # log.debugv('- Size T: {}'.format(target_dataset.sum()))
    for k in generated_dataset.index:
        # log.debugv("  - Class size of k = {} for G: {}".format(k, generated_dataset.get(k)))
        # log.debugv("  - Class size of k = {} for T: {}".format(k, target_dataset.get(k)))
        t_delta = get_t_delta(generated_dataset.get(k), generated_dataset.sum(),
                              target_dataset.get(k), target_dataset.sum())
        abs_delta_G_T[k] = abs(t_delta)
        delta_G_T[k] = t_delta
        # log.debugv('- delta G T for k = {}: {}'.format(k, t_delta))
    return delta_G_T, abs_delta_G_T


def gmd(list_delta):
    """
    Get max 't delta' from list
    :param list_delta:
    :return: Max delta
    """
    return max(list_delta.values())


def get_arg_max_min(list_delta):
    """
    The get arg max and arg min classes from a list of deltas
    :param list_delta:
    :return: A float, and a float
    """
    return max(list_delta, key=list_delta.get), min(list_delta, key=list_delta.get)


def algo1_debias_by_delta(base_dataset_o, target_dataset, source_dataset_o, delta, base_sizes=None, source_sizes=None):
    """
    Loop through all the classes in the generated dataset, adding applications to underrepresented classes and removing
    application from overrepresented ones.

    :param base_dataset: An object of classs Dataset
    :param target_dataset: An object of classs Dataset
    :param source_dataset: An object of classs Dataset
    :param delta: Maximum difference allowed for the difference between class proportion between the generated dataset and
    the target dataset (a float)
    :return: generated_dataset
    """

    # G <- B
    b_sizes = copy_these_sizes(base_dataset_o.class_size)
    s_sizes = copy_these_sizes(source_dataset_o.class_size)

    generated_dataset = Dataset(base_dataset_o.items, b_sizes)
    source_dataset = Dataset(base_dataset_o.items, s_sizes)

    # log.debugv("b is base_dataset_o.items? {}".format(b is base_dataset_o.items))
    # log.debugv("generated_dataset is base_dataset_o? {}".format(base_dataset_o is generated_dataset))
    # log.debugv("generated_dataset.items is base_dataset_o.items? {}"
    #            .format(base_dataset_o.items is generated_dataset.items))

    # Generate dictionary
    delta_G_T, abs_delta_G_T = getme_deltas(generated_dataset, target_dataset)
    max_delta = gmd(abs_delta_G_T)

    class_max_delta = "?"
    for k in abs_delta_G_T:
        if abs_delta_G_T[k] == max_delta:
            class_max_delta = k
            break

    n_runs = 0

    no_kl = False
    while max_delta > delta:
        n_runs += 1
        # log.debugv("* max_delta {} (class {}) > {} (Run {} Algo 1)".format(max_delta, class_max_delta, delta, n_runs))

        k_h, k_l = get_arg_max_min(delta_G_T)
        # log.debugv("- k_h = {}".format(k_h))
        # log.debugv("- k_l = {}".format(k_l))

        if k_l in source_dataset.index:
            if source_dataset.get(k_l) == 0:
                no_kl = True
        else:
            no_kl = True

        if no_kl:
            # log.debugv("{} not in source_dataset".format(k_l))
            # log.debugv("* No more apps of class {} in source_dataset".format(k_l))
            list_k = source_dataset.get_non_empty()
            delta_k_l = get_t_delta(generated_dataset.get(k_l), generated_dataset.sum(),
                                    target_dataset.get(k_l), target_dataset.sum())
            if delta_k_l >= -delta and len(list_k) != 0:
                k_l = class_arg_max_min(generated_dataset, target_dataset, list_k=list_k)[1]
                no_kl = False
                # log.debug("- new k_l = {}".format(k_l))
            else:
                # if delta_k_l >= -delta:
                #     log.debug("* Impossible to create: delta k_h < -delta (delta = {}, delta k_l {})"
                #               .format(delta, delta_k_l))
                # elif len(list_k) != 0:
                #     log.debug("* Impossible to create: no more k_l (underepresented) classes in source")

                # log.debug("* Can't go forward, giving \"Impossible\" (run = {}, delta k_l = {}, # k_l = {})"
                #           .format(n_runs, delta_k_l, len(list_k)))
                return None

        # Add one element of k_l in G
        source_dataset.update(k_l, -1)
        generated_dataset.update(k_l, 1)

        # log.debugv('- Removing one app from S of class {} ({} remain)'.format(k_l, source_dataset.s.get(k_l)))
        # log.debugv('- Adding one app to G of class {} ({} in total)'.format(k_l, generated_dataset.s.get(k_l)))

        # Remove one element of k_h in G
        generated_dataset.update(k_h, -1)
        source_dataset.update(k_h, 1)

        # log.debugv('- Removing one app from G of class {} ({} remain)'.format(k_h, generated_dataset.s.get(k_h)))
        # log.debugv('- Adding one app to S of class {} ({} in total)'.format(k_h, source_dataset.s.get(k_h)))

        delta_k_h = get_t_delta(generated_dataset.get(k_h), generated_dataset.sum(),
                                target_dataset.get(k_h), target_dataset.sum())
        delta_k_l = get_t_delta(generated_dataset.get(k_l), generated_dataset.sum(),
                                target_dataset.get(k_l), target_dataset.sum())

        if delta_k_h < -delta or delta_k_l > delta:
            # if delta_k_h < -delta:
            #     log.debug("* Impossible to create: delta k_h < -delta (delta = {}, delta k_h = {}, delta k_l {}"
            #               .format(delta, delta_k_h, delta_k_l))
            # if delta_k_l > delta:
            #     log.debug("* Impossible to create: delta k_l > delta (delta = {}, delta k_h = {}, delta k_l {}"
            #               .format(delta, delta_k_h, delta_k_l))
            return None

        # log.debug("- Getting max_delta")
        delta_G_T[k_h] = delta_k_h
        delta_G_T[k_l] = delta_k_l
        abs_delta_G_T[k_h] = abs(delta_k_h)
        abs_delta_G_T[k_l] = abs(delta_k_l)

        max_delta = gmd(abs_delta_G_T)

    class_max_delta = "?"
    for k in abs_delta_G_T:
        if abs_delta_G_T[k] == max_delta:
            class_max_delta = k
            break
    # log.debug("* max_delta {} (class {}) < {}, exit".format(max_delta, class_max_delta, delta))
    return generated_dataset


def n_plus(B_k, size_B, T_k, size_T, delta):
    """
    Get the minimal number of removals to make class 'k' representative
    :param B_k: # apps of class k in the base dataset
    :param size_B: Size of base dastaset
    :param T_k: # apps of class k in the target dataset
    :param size_T: Size of target dataset
    :param delta: Maximum difference allowed for the difference between class proportion between the generated dataset and
    the target dataset (a float)
    :return: A float
    """
    delta_t = get_t_delta(B_k, size_B, T_k, size_T)
    return np.maximum(0, np.ceil(size_B * (delta_t - delta)))


def n_minus(B_k, size_B, T_k, size_T, delta):
    """
    Get the minimal number of additions to make class 'k' representative
    :param B_k: # apps of class k in the base dataset
    :param size_B: Size of base dastaset
    :param T_k: # apps of class k in the target dataset
    :param size_T: Size of target dataset
    :param delta: Maximum difference allowed for the difference between class proportion between the generated dataset and
    the target dataset (a float)
    :return: A float
    """
    delta_t = get_t_delta(B_k, size_B, T_k, size_T)
    return np.maximum(0, np.ceil(-size_B * (delta_t + delta)))


def two_max_n_plus_minus(base_dataset, target_dataset, delta):
    """
    Get 2 times the max of n+ and n- for all classes
    :param base_dataset: An object of class Dataset
    :param target_dataset: An object of class Dataset
    :param delta: Maximum difference allowed for the difference between class proportion between the generated dataset and
    the target dataset (a float)
    :return: A float
    """
    sum_n_plus = 0
    sum_n_minus = 0

    # log.debugv("- Calculating two max n plus minus")
    for index in base_dataset.index:
        B_k = base_dataset.get(index)
        size_B = base_dataset.sum()
        T_k = target_dataset.get(index)
        size_T = target_dataset.sum()

        sum_n_plus = sum_n_plus + n_plus(B_k, size_B, T_k, size_T, delta)
        sum_n_minus = sum_n_minus + n_minus(B_k, size_B, T_k, size_T, delta)
        # log.debugv("  - sum n plus for k = {}: {}".format(index, sum_n_plus))
        # log.debugv("  - sum n minus for k = {}: {}".format(index, sum_n_minus))

    tmnpm = 2 * np.maximum(sum_n_plus, sum_n_minus)
    # log.debugv("- Result two max n plus minus: {}".format(tmnpm))
    return tmnpm


def get_best_solution(generated_dataset, source_dataset, base_dataset):
    """
    Add and remove apps in the generated dataset based on the difference of class sizes between it and the base dataset
    :param generated_dataset: An object of class Dataset
    :param source_dataset: An object of class Dataset
    :param base_dataset: An object of class Dataset
    :return: The modified generated_dataset
    """
    # For all classes in the generated dataset
    for k in generated_dataset.index:
        num_changes = generated_dataset.class_size[k] - base_dataset.class_size[k]
        if num_changes > 0:
            # Add apps from source to generated dataset
            for n in range(num_changes):
                this_hash, ki = source_dataset.remove_one_app(k)
                generated_dataset.add_one_app(this_hash, ki)

        elif num_changes < 0:
            # Remove apps from generated dataset
            for n in range(abs(num_changes)):
                generated_dataset.remove_one_app(k)

    # Remove those classes from items that are empty (based on the dataset's class_size)
    for k in list(generated_dataset.items):
        if generated_dataset.class_size[k] == 0:
            del generated_dataset.class_size[k]
            del generated_dataset.items[k]
    return generated_dataset


def d_H_class_size(base_dataset, generated_dataset):
    if base_dataset is None:
        return None
    diff_G_B = 0
    for k in generated_dataset.class_size:
        diff_G_B += abs(generated_dataset.class_size[k] - base_dataset.class_size[k])
    return diff_G_B


def d_H_items(generated_dataset, base_dataset):
    set_G = []
    set_B = []
    for k in generated_dataset.items:
        set_G.extend(generated_dataset.items[k])
    set_G = set(set_G)

    for k in base_dataset.items:
        set_B.extend(generated_dataset.items[k])
    set_B = set(set_B)

    diff_G_B = set_G.difference(set_B)
    diff_B_G = set_B.difference(set_G)
    return len(diff_B_G.union(diff_G_B))


def algo2_find_best_solution(base_dataset, target_dataset, source_dataset, delta, change_dataset_at_runtime=False):
    """
    Get best generated dataset from base dataset to target dataset using source dataset
    :param base_dataset: An object of class Dataset
    :param target_dataset: An object of class Dataset
    :param source_dataset: An object of class Dataset
    :param delta: Maximum difference allowed for the difference between class proportion between the generated dataset and
    the target dataset (a float)
    :return:
    """

    log.info("Finding first best solution")
    t_first_sol = time.time()

    d_H_B_T = d_H_class_size(base_dataset, target_dataset)
    log.info("- d_H of base and target dataset: {}".format(d_H_B_T))
    # Check if base dataset is modified (it shouldn't)
    res = algo1_debias_by_delta(base_dataset, target_dataset, source_dataset, delta)

    # algo1 may return a Dataset object (a modified base dataset)
    # or it may return None
    if res is not None:
        d_min = two_max_n_plus_minus(base_dataset, target_dataset, delta)
        best_solution = res
    else:
        log.warning("Algo1 returned impossible at the very first run")
        d_min = np.PINF
        best_solution = None

    t_now = time.time()
    elapsed_time = str(datetime.timedelta(seconds=t_now - t_first_sol))
    log.info("Run completed (time spent: {})".format(elapsed_time))

    if best_solution is not None:
        d_H_B_G = d_H_class_size(base_dataset, best_solution)
    else:
        d_H_B_G = np.PINF
    log.info("Finding best solution up to |B| - d_min")
    log.info("- d_min: {}".format(d_min))
    log.info("- d_H(B,G): {}".format(d_H_B_G))
    # log.info("- d_min/2: {}".format(d_min/2))
    log.info("- |B|: {}".format(base_dataset.sum()))
    # log.info("- |G|: {}".format(best_solution.sum()))
    # exit(43)

    ################################################################################
    #                                                                              #
    #                            Algorithm 2 first loop                            #
    #                                                                              #
    ################################################################################

    generated_dataset = copy.deepcopy(base_dataset)
    b_sum = base_dataset.sum()

    num_ops = 0

    current_run = 0
    num_run = base_dataset.sum() - 1 - (np.maximum(1, base_dataset.sum() - d_min) - 1)
    for n in np.arange(base_dataset.sum() - 1, np.maximum(1, base_dataset.sum() - d_min) - 1, -1):
        current_run += 1
        if current_run > d_min:
            break
        k_h = class_arg_max_min(generated_dataset, target_dataset)[0]

        t_second_phase_run = time.time()

        # Remove one element of k_h in G
        # log.debug("Removing one app of class: {}".format(k_h))
        generated_dataset.update(k_h, -1)

        # log.debugv("- Generated size at this run: {}".format(generated_dataset.sum()))
        # log.debugv("- Source size at this run: {}".format(source_dataset.sum()))
        # log.debugv("- Class sizes at this run:")

        d = base_dataset.sum() - n + two_max_n_plus_minus(generated_dataset, target_dataset, delta)
        algo1_res = algo1_debias_by_delta(generated_dataset, target_dataset, source_dataset, delta)

        if d < d_min and algo1_res is not None:
            d_min = d
            d_H_B_G = d_H_class_size(base_dataset, algo1_res)
            # log.info("- New d_min: {}".format(d_min))
            best_solution = algo1_res

        # t_now = time.time()
        # elapsed_time = str(datetime.timedelta(seconds=t_now - t_second_phase_run))
        # log.info("Algorithm 2 first loop: \nRun {} (of {}) in {}\n(n = {}, d = {}, d_min = {}, dH(B,G) = {})"
        #          .format(current_run, num_run, elapsed_time, n, d, d_min, d_H_B_G))

        num_ops += 1
        if num_ops % 1000 == 0:
            t_now = time.time()
            elapsed_time = str(datetime.timedelta(seconds=t_now - t_first_sol))
            log.info("{:,} operations done so far. Time elapsed: {}".format(num_ops, elapsed_time))
            log.info("Algorithm 2 first loop: \nRun {} (of {})\n(n = {}, d = {}, d_min = {}, dH(B,G) = {})"
                     .format(current_run, num_run, n, d, d_min, d_H_B_G))

    ################################################################################
    #                                                                              #
    #                           Algorithm 2 second loop                            #
    #                                                                              #
    ################################################################################

    log.info("Finding best solution up to d_min")
    log.info("- d_min: {}".format(d_min))
    log.info("- |B|: {}".format(base_dataset.sum()))
    generated_dataset = copy.deepcopy(base_dataset)

    current_run = 0
    num_run = (base_dataset.sum() + d_min + 1) - (base_dataset.sum() + 1)

    if d_min == np.PINF:
        second_loop_stop = base_dataset.sum() + source_dataset.sum()
    else:
        second_loop_stop = base_dataset.sum() + d_min

    no_kl = False
    for n in np.arange(base_dataset.sum() + 1, second_loop_stop + 1):
        current_run += 1
        if current_run > d_min:
            break
        t_third_phase_run = time.time()
        # log.info("Third phase: Run {} (of {}), (n = {})".format(current_run, num_run, n))
        k_l = class_arg_max_min(generated_dataset, target_dataset)[1]

        # Check if there are apps left of class k_l in source dataset
        if k_l in source_dataset.index:
            if source_dataset.get(k_l) == 0:
                no_kl = True
        else:
            no_kl = True

        if no_kl:
            log.debug("No more apps of class {} in source_dataset (run {})".format(k_l, current_run))
            list_k = source_dataset.get_non_empty()

            delta_k_l = get_t_delta(generated_dataset.get(k_l), generated_dataset.sum(),
                                    target_dataset.get(k_l), target_dataset.sum())

            if delta_k_l >= -delta and len(list_k) != 0:
                k_l = class_arg_max_min(generated_dataset, target_dataset, list_k=list_k)[1]
                no_kl = False
                # log.debug("- new k_l = {}".format(k_l))
            else:
                # If there no more applications left, break
                break

        # Add one element of k_h in G
        # log.debug("Adding one app of class: {}".format(k_l))
        source_dataset.update(k_l, -1)
        generated_dataset.update(k_l, 1)

        # log.debugv("- Generated size at this run: {}".format(generated_dataset.sum()))
        # log.debugv("- Source size at this run: {}".format(source_dataset.sum()))
        # log.debugv("- Class sizes at this run:")

        # log.debug('Removing one app from S of class {} ({} remain)'.format(k_l, source_dataset.get(k_l)))
        # log.debug('Adding one app to G of class {} ({} in total)'.format(k_l, generated_dataset.get(k_l)))

        two_max_npm = two_max_n_plus_minus(generated_dataset, target_dataset, delta)
        d = n - b_sum + two_max_npm
        # log.debugv("d ({}) <- n ({}) - |B| ({}) + 2max(sum_k n+(k), sum_k n-(k)) ({})"
        #            .format(d, n, b_sum, two_max_npm))
        algo1_res = algo1_debias_by_delta(generated_dataset, target_dataset, source_dataset, delta)

        if d < d_min and algo1_res is not None:
            d_min = d
            # log.info("- New d_min: {}".format(d_min))
            best_solution = algo1_res

        # t_now = time.time()
        # elapsed_time = str(datetime.timedelta(seconds=t_now - t_third_phase_run))
        # log.info("Algorithm 2 second loop: \nRun {} (of {}) in {}\n(n = {}, d = {}, d_min = {}, dH(B,G) = {})"
        #          .format(current_run, num_run, elapsed_time, n, d, d_min, d_H_B_G))

        num_ops += 1
        if num_ops % 1000 == 0:
            t_now = time.time()
            elapsed_time = str(datetime.timedelta(seconds=t_now - t_first_sol))
            # diff_prev = str(datetime.timedelta(seconds=t_now - t_previous))
            log.info("{:,} operations done so far. Time elapsed: {}".format(num_ops, elapsed_time))
            log.info("Algorithm 2 second loop: \nRun {} (of {})\n(n = {}, d = {}, d_min = {}, dH(B,G) = {})"
                     .format(current_run, num_run, n, d, d_min, d_H_B_G))

    log.info("Algorithm 2 finished with {:,} operations. d_min = {}".format(num_ops, d_min))

    # Based on the best solution configuration (difference of # of apps of each class the best solution
    # and the base dataset)
    # Remove apps from or add apps to the best solution
    if best_solution is not None:
        best_solution = get_best_solution(best_solution, source_dataset, base_dataset)

    return best_solution, d_min


def get_combinations_from_datasets_dsp(datasets_list, list_classes, ignore_classes=False):
    # Get the datasets
    dataset_dict = {}

    # dataset_list are '*.combination_hashes.json' files
    for dataset_path in datasets_list:
        combination_hashes = utilities.load_json_file(dataset_path)
        for combination in combination_hashes:
            combination_tuple = combination.strip()
            if combination_tuple not in list_classes and not ignore_classes:
                continue
            if combination_tuple not in dataset_dict:
                dataset_dict[combination_tuple] = []

            this_combination = dataset_dict[combination_tuple]
            for app_hash in combination_hashes[combination]:
                if app_hash not in this_combination:  # No copies in the same class
                    this_combination.append(app_hash)

    return dataset_dict


def remove_copies(source, base):
    for combination in base:
        base_hashes = base[combination]
        source_hashes = source[combination]

        for h in base_hashes:
            if h in source_hashes:
                source_hashes.remove(h)
    return source


def wrapper_algo_search_plus(base_datasets_list, base_dataset_name, base_datasets_id,
                             target_dataset_path, target_dataset_name, target_dataset_id,
                             source_datasets_list, source_datasets_id, delta,
                             output_dir, ds_id, feat_list_name, name=None, get_dmin=False):
    """
    Wrapper function for the debiasing algorithm 1 and 2

    Parameters
    ----------
    base_datasets_list: A list of paths to '*.combination_hashes.json' files of the base datasets
    base_dataset_name
    base_datasets_id
    target_dataset_path: A path to the '*.combination_hashes.json' target dataset file
    target_dataset_name
    target_dataset_id
    source_datasets_list
    source_datasets_id
    delta
    output_dir
    ds_id
    feat_list_name
    name
    get_dmin

    Returns
    -------

    """
    log.debugv("Executing wrapper algo search plus")
    # Generated dataset directory
    output_dir = output_dir + '/' + ds_id

    # Create output directory (same name as the id)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Check if already done
    if ds_id:
        output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.dataset_class_info.json'
    else:
        output_file = output_dir + '/generated_dataset_algo_search_plus_' + now_str + '.dataset_class_info.json'

    if os.path.exists(output_file):
        dataset_info = utilities.load_json_file(output_file)

        if 'impossible' in dataset_info:
            is_impossible = dataset_info['impossible']
            if is_impossible:
                log.warning("Debias {} to {} at delta {} already done and impossible. Exiting"
                            .format(base_dataset_name, target_dataset_name, delta))
                exit(999)

    # Load base dataset
    if len(base_datasets_list) == 1:
        base_dataset_combinations = utilities.load_json_file(base_datasets_list[0])
    else:
        base_dataset_combinations = get_combinations_from_datasets_dsp(base_datasets_list, [], ignore_classes=True)

    # Load target dataset
    target_dataset_combinations = utilities.load_json_file(target_dataset_path)

    # Getting all classes
    log.debug("Getting classes")
    all_classes = list(set(list(target_dataset_combinations.keys())
                           + list(base_dataset_combinations.keys())))

    # Load source dataset, but only those classes in base and target dataset
    log.debug("Loading sources")
    source_dataset_combinations = get_combinations_from_datasets_dsp(source_datasets_list, all_classes)

    log.debug("Getting combinations")
    base_dataset_series = {}
    target_dataset_series = {}
    source_dataset_series = {}

    # Create a dictionary of the # of apps per class for base, target and source,
    for k in all_classes:
        if k not in base_dataset_combinations:
            base_dataset_combinations[k] = []
        base_dataset_series[k] = len(base_dataset_combinations[k])

        if k not in target_dataset_combinations:
            target_dataset_combinations[k] = []
        target_dataset_series[k] = len(target_dataset_combinations[k])

        if k not in source_dataset_combinations:
            source_dataset_combinations[k] = []
        source_dataset_series[k] = len(source_dataset_combinations[k])

    # Create the dataset objects
    log.debug("Creating object base_dataset")
    log.debug("# keys Series base dataset: {}".format(len(base_dataset_series.keys())))
    base_dataset = Dataset(base_dataset_combinations)
    log.debug("  Size: {}".format(base_dataset.total))
    log.debug("# items keys base dataset: {}".format(len(base_dataset.items.keys())))
    log.debug("# indices base dataset: {}".format(len(base_dataset.index)))

    log.debug("Creating object target_dataset")
    target_dataset = Dataset(target_dataset_combinations)
    log.debug("  Size: {}".format(target_dataset.total))
    log.debug("# classes target dataset: {}".format(len(target_dataset.items.keys())))
    log.debug("# indices target dataset: {}".format(len(target_dataset.index)))

    if get_dmin:
        d_min_res = d_H_class_size(base_dataset, target_dataset)
        percent_modifs = d_min_res/target_dataset.sum()
        log.info("dH(B, T) = {}".format(d_min_res))
        log.info("Percent of modifs: {}".format(percent_modifs))
        if ds_id:
            output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.dataset_class_info.json'
        else:
            output_file = output_dir + '/generated_dataset_algo_search_plus_' + now_str + '.dataset_class_info.json'

        if os.path.exists(output_file):
            dataset_info = utilities.load_json_file(output_file)
            dataset_info['d_min final'] = d_min_res
            dataset_info['percent modifs'] = percent_modifs

            utilities.dump_json_to_file(dataset_info, output_file)
        exit(44)

    # Removing copies of base dataset from source
    log.debug("Removing copies of base dataset from source")
    source_dataset_combinations = remove_copies(source_dataset_combinations, base_dataset_combinations)

    log.debug("Creating object source_dataset")
    source_dataset = Dataset(source_dataset_combinations)

    log.debug("Source size: {}".format(source_dataset.sum()))

    # *.dataset_class_info.json structure
    output_json = {
        'name': name,
        'size': None,
        'modified': True,
        'base datasets': base_datasets_id,
        'original size': base_dataset.sum(),
        'target dataset': target_dataset_id,
        'source dataset': source_datasets_id,
        'source size': source_dataset.sum(),
        'delta': delta,
        'number combinations': 0,
        'debiasable': False,
        'added': 0,
        'removed': 0,
        'd_min final': 0,
        'percent modifs': 0,
        'add ratio': 0,
        'run time': None
    }

    # Calling algorithm 2
    log.debug("Calling Algo2")
    t_start = time.time()
    generated_dataset, d_min = algo2_find_best_solution(base_dataset, target_dataset, source_dataset, delta)
    t_now = time.time()
    elapsed_time = str(datetime.timedelta(seconds=t_now - t_start))
    log.info("Run completed (time spent: {})".format(elapsed_time))

    if generated_dataset is None:
        log.warning("Impossible to debias {} to {} at delta = {}".format(base_dataset_name, target_dataset_name, delta))
        output_json['run time'] = elapsed_time
    else:
        log.debug("We got something, writing")
        output_json['debiasable'] = True
        output_json['run time'] = elapsed_time

        # Get only the combinations hashes from generated dataset
        generated_combinations = generated_dataset.items

        output_json['size'] = generated_dataset.sum()
        log.debug("Generated dataset size: {}".format(generated_dataset.sum()))
        output_json['number combinations'] = len(generated_dataset.items)
        output_json['d_min final'] = d_min
        output_json['percent modifs'] = d_min/base_dataset.sum()

        """
          nb ajout = ( |G| - |B| + d_min) / 2
        nb retrait = (-|G| + |B| + d_min) / 2
        """

        output_json['added'] = (generated_dataset.sum() - base_dataset.sum() + d_min) / 2
        output_json['removed'] = (-generated_dataset.sum() + base_dataset.sum() + d_min) / 2
        output_json['add ratio'] = output_json['added'] / output_json['size']

        # Output combinations hashes
        if ds_id:
            output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.combination_hashes.json'
        else:
            output_file = output_dir + '/generated_dataset_algo_cool' + now_str + '.combination_hashes.json'
        utilities.dump_json_to_file(generated_combinations, output_file)
        log.info("Generated dataset combinations_hashes saved as {}".format(output_file))

        # Output *.classcount.csv file
        output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.classcount.csv'
        ci.combinations_hashes_to_classcount(generated_combinations, output_dir, ds_id,
                                             output_file, feat_file_name=feat_list_name)
        log.info("Generated dataset classcount saved as {}".format(output_file))

    # Output dataset_class_info file
    if ds_id:
        output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.dataset_class_info.json'
    else:
        output_file = output_dir + '/generated_dataset_algo_search_plus_' + now_str + '.dataset_class_info.json'

    utilities.dump_json_to_file(output_json, output_file, is_numpy=True)
    log.info("Dataset info saved as {}".format(output_file))
    dataset_config_specs = "[{}]\ndataset_name={}\ncamembert_name={}\n\n".format(ds_id, name, "${dataset_name}")

    if generated_dataset is not None:
        return generated_dataset.items, dataset_config_specs
    else:
        exit(334)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a new dataset based on one (base) to imitate another "
                                                 "(target)")
    parser.add_argument('config_file', help='Full path to the config file')
    parser.add_argument('id', help='New dataset id')
    parser.add_argument('name', help='New dataset full name')
    parser.add_argument('-f', '--feature-file',
                        help='Path to the selected features files (this overwrites the one in the config file)')
    parser.add_argument('--base-datasets', required=True, nargs='+', help='Identifier of the base datasets')
    parser.add_argument('--target-dataset', required=True, help='Identifier of the target dataset')
    parser.add_argument('--source-datasets', nargs='+', help='Identifiers of the source datasets')
    parser.add_argument('--delta', type=float, help='Max error to tolerate (Float)')
    parser.add_argument('-o', '--overwrite', action='store_true',
                        help='Overwrite the current generated dataset (if it exists)')

    parser.add_argument('--get-dH', action='store_true', help='Calculate the dH between base and target dataset')
    parser.add_argument('-v', action="count", help='Output information to the standard output (-vv is very verbose)')

    args = parser.parse_args()

    if args.v is not None:
        verbosity = args.v
        print("Verbosity: " + str(verbosity))
    else:
        verbosity = 0

    logSetup(verbosity, args.base_datasets, args.target_dataset, args.delta)

    if not os.path.exists(args.config_file):
        log.error("{} cannot be found. Exiting".format(args.config_file))
        exit(404)

    config = configparser.ConfigParser(default_section='general', interpolation=configparser.ExtendedInterpolation())
    config.read(args.config_file)

    general_opts = config.defaults()
    log.debugv("general opts: {}".format(general_opts))
    for elem in ['res_dir', 'visual_output']:
        if elem not in general_opts:
            log.error("{} not specified in config file. Exiting".format(elem))
            exit(11)

    # Extract feature file
    feature_file_path = ""
    if args.feature_file:
        if os.path.exists(args.feature_file):
            feature_file_path = os.path.expanduser(args.feature_file)
        else:
            log.error("Path \"{}\" doesn't exists. Exiting".format(args.feature_file))
            exit(432)
    else:
        feature_file_path = os.path.expanduser(config.get('general', 'selected_features_file'))
    feat_list_name = '.'.join(feature_file_path.split('.')[:-1])

    # Input and output dirs
    res_dir = os.path.expanduser(config.get('general', 'res_dir'))
    input_dataset_dir = os.path.expanduser(config.get('general', 'input_dataset_dir'))

    output_dir = os.path.expanduser(res_dir)

    log.debug("Get dH: {}".format(args.get_dH))
    if not args.overwrite and not args.get_dH:
        if os.path.exists(res_dir + '/' + args.id + '/' + args.id + '.' + feat_list_name + '.dataset_class_info.json'):
            log.warning("The dataset class info file already exist (in \"{}\"), not doing anything"
                        .format(res_dir + '/' + args.id))
            exit(302)

    feature_list_path = os.path.expanduser(config.get('general', 'selected_features_file'))

    sections = config.sections()
    for elem in args.base_datasets + [args.target_dataset]:
        if elem not in sections:
            log.error("Dataset \"{}\" not specified in config file. Exiting".format(elem))
            exit(11)

    # ------------------------
    log.debugv("Getting base datasets")
    # ------------------------

    base_combination_hashes_list = []
    base_characteristics_path_list = []
    base_datasets_id = []

    if args.base_datasets:
        for elem in args.base_datasets:
            if elem not in sections:
                log.error("Dataset \"{}\" not specified in config file. Exiting".format(elem))
                exit(11)

        for base_dataset in args.base_datasets:

            dataset_found = False

            for dataset_dir in [input_dataset_dir, res_dir]:
                combination_hashes_file = os.path.expanduser(dataset_dir + '/' + base_dataset
                                                             + '/' + base_dataset + '.' + feat_list_name
                                                             + '.combination_hashes.json')
                if os.path.exists(combination_hashes_file):
                    base_combination_hashes_list.append(combination_hashes_file)
                else:
                    log.warning("Can't continue without the {} combinations_hashes file ({} not found). "
                                "Searching elsewhere"
                                .format(base_dataset, combination_hashes_file))
                    continue

                characteristics_path = os.path.expanduser(dataset_dir + '/' + base_dataset
                                                          + '/' + base_dataset + '.characteristics.csv')
                if os.path.exists(characteristics_path):
                    base_characteristics_path_list.append(characteristics_path)
                else:
                    log.warning("Can't continue without the {} characteristics file ({} not found). "
                                "Searching elsewhere."
                                .format(base_dataset, characteristics_path))
                    continue

                log.info("Using for base the \"{}\" combination hashes file found in \"{}\""
                         .format(base_dataset, combination_hashes_file))
                log.info("Using for base the \"{}\" characteristics file found in \"{}\""
                         .format(base_dataset, characteristics_path))
                base_datasets_id.append(base_dataset)
                dataset_found = True
                break

            if not dataset_found:
                log.error("Can't continue without the {} combinations_hashes or the characteristics file."
                          .format(base_dataset, combination_hashes_file))
                exit(444)
    else:
        log.error("No base, no debias. Exiting")
        exit(408)

    base_dataset_name = "_".join(args.base_datasets)

    log.debugv("Using these datasets as base: {}".format(base_combination_hashes_list))

    # --------------------------
    log.debugv("Getting target dataset")
    # --------------------------

    target_dataset_path = os.path.expanduser(input_dataset_dir + '/' + args.target_dataset + '/' +
                                             args.target_dataset + '.' + feat_list_name + '.combination_hashes.json')

    if not os.path.exists(target_dataset_path):
        log.error("Combination hashes for target dataset not found in \"{}\"".format(input_dataset_dir))
        exit(13)

    target_dataset_name = config.get(args.target_dataset, 'dataset_name', vars=None)
    log.debug("Using {} as target dataset".format(args.target_dataset))

    # -----------------------------------
    log.debugv("Getting source datasets")
    # -----------------------------------

    sources_dataset_list = []
    sources_matrix_path_list = base_characteristics_path_list
    source_datasets_id = []
    sources_characteristics_path_list = []

    if args.source_datasets:
        for elem in args.source_datasets:
            if elem not in sections:
                log.error("{} not specified in config file. Exiting".format(elem))
                exit(11)

        for source_dataset in args.source_datasets:
            # log.info("Using {} as a source dataset".format(source_dataset))
            source_datasets_id.append(source_dataset)

            dataset_found = False

            for dataset_dir in [input_dataset_dir, res_dir]:
                combination_hashes_file = os.path.expanduser(dataset_dir + '/' + source_dataset
                                                             + '/' + source_dataset + '.' + feat_list_name
                                                             + '.combination_hashes.json')
                if os.path.exists(combination_hashes_file):
                    sources_dataset_list.append(combination_hashes_file)
                else:
                    log.warning("Can't continue without the {} combinations_hashes file ({} not found). "
                                "Searching elsewhere"
                                .format(source_dataset, combination_hashes_file))
                    continue

                characteristics_path = os.path.expanduser(dataset_dir + '/' + source_dataset
                                                          + '/' + source_dataset + '.characteristics.csv')
                if os.path.exists(characteristics_path):
                    sources_characteristics_path_list.append(characteristics_path)
                else:
                    log.warning("Can't continue without the {} characteristics file ({} not found). "
                                "Searching elsewhere."
                                .format(source_dataset, characteristics_path))
                    continue

                log.info("Using for source the \"{}\" combination hashes file found in \"{}\""
                         .format(source_dataset, combination_hashes_file))
                log.info("Using for source the \"{}\" characteristics file found in \"{}\""
                         .format(source_dataset, characteristics_path))
                dataset_found = True
                break

            if not dataset_found:
                log.error("Cannot continue without the {} combinations_hashes file ({} not found)."
                          .format(source_dataset, combination_hashes_file))
                exit(404)
    else:
        log.error("No sources specified. Exiting")
        exit(408)

    log.info("Creating new dataset with id: {}, name \"{}\"".format(args.id, args.name))
    generated_dataset, new_config_lines = wrapper_algo_search_plus(base_combination_hashes_list, base_dataset_name, base_datasets_id,
                                                                   target_dataset_path, target_dataset_name, args.target_dataset,
                                                                   sources_dataset_list, source_datasets_id,
                                                                   args.delta, output_dir,
                                                                   args.id, feat_list_name, args.name, get_dmin=args.get_dH)

    if args.id not in sections:
        with open(args.config_file, 'a') as out:
            out.write(new_config_lines)

    utilities.generate_characteristics_file(generated_dataset, base_characteristics_path_list
                                            + sources_characteristics_path_list, output_dir, args.id)

    quit()
