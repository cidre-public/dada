import argparse
import logging
import os
import sys


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning(
            'Logging level "{}" not defined, setting "normal" instead'.format(level)
        )


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style["debugv"] = {"color": "magenta"}
        coloredlogs.install(
            show_hostname=False,
            show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt="%(asctime)s [%(levelname)8s] %(message)s"
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()

# Function for converting arff list to csv list
def toCsv(text):
    data = False
    header = ""
    new_content = []
    string_cols = []
    col_num = 0
    for line in text:
        if not data:
            if "@ATTRIBUTE" in line or "@attribute" in line:
                attributes = line.split()
                if "@attribute" in line:
                    attri_case = "@attribute"
                else:
                    attri_case = "@ATTRIBUTE"
                column_name = attributes[attributes.index(attri_case) + 1]
                header = header + column_name + ","
                type = attributes[-1]
                if type == "string":
                    string_cols.append(col_num)
                col_num += 1
            elif "@DATA" in line or "@data" in line:
                data = True
                header = header[:-1]
                header += "\n"
                new_content.append(header)
        else:
            if len(string_cols) == 0:
                new_content.append(line)
            else:
                cols = line.split(",")
                for col_index in string_cols:
                    cols[col_index] = cols[col_index].replace("'", "")
                new_content.append(",".join(cols))

    return new_content


def arff_to_csv(input_filename_list, output_dirname):
    if not os.path.exists(output_dirname):
        log.info("Path {} doesn't exists, creating it".format(output_dirname))
        os.makedirs(output_dirname)

    for input_filename in input_filename_list:
        if not os.path.exists(input_filename):
            log.error("File {} doesn't exists, exiting".format(input_filename))
            exit(-1)

        with open(input_filename, "r") as inFile:
            content = inFile.readlines()
            # name = ".".join(
            #     os.path.basename(os.path.normpath(input_filename)).split(".")[:-1]
            # )
            name = os.path.basename(os.path.normpath(input_filename)).split(".")[0]
            log.debug("name: {}".format(name))
            new = toCsv(content)
            output_csv = os.path.join(
                output_dirname, name + ".graph_characteristics.csv"
            )
            with open(output_csv, "w") as outFile:
                outFile.writelines(new)
            log.info("File saved as {}".format(output_csv))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Arff to CSV script")
    parser.add_argument("input_files", nargs="+", help="Full path of output file")
    parser.add_argument(
        "-o", "--output-dir", required=True, help="The path to the output"
    )
    parser.add_argument(
        "-v",
        help="Output information to the standard output (-vv is very verbose)",
        action="count",
    )

    args = parser.parse_args()

    if args.v is not None:
        verbosity = args.v
        print("Verbosity: " + str(verbosity))
    else:
        verbosity = 0

    logSetup(verbosity)
    # log.debug("This is a debug print")
    # log.debugv("This is a very verbose debug print")
    # call my function
    arff_to_csv(args.input_files, args.output_dir)

    quit()
