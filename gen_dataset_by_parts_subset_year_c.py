import argparse
import configparser
import logging
import os
import time
import pandas
from tabulate import tabulate

import utilities as ut
from count_instances import combinations_hashes_to_classcount
from gen_dataset_by_parts import gen_dataset_by_parts

now_str = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def gen_datasets_mwgw_year_window(dataset_a, dataset_b, limit_year,
                                  dataset_a_characteristics_path, dataset_b_characteristics_path, id, name,
                                  res_dir, feat_list_name, percent=5, date_fix=None, balance_time_window=True):
    year_list = list(range(1970, 2021+1))
    log.debug("Using folowing year: {}".format(year_list))

    dataset_by_year = [{}, {}]
    for y in year_list:
        for dict in dataset_by_year:
            dict[y] = {}

    if date_fix:
        log.debug("- Using date_fix")
        date_fix_df = pandas.read_csv(date_fix).fillna(0)

    # Load characteristics files
    log.info("Loading characteristics files")
    characteristics_year_list = []
    for d_characteristics_path in [dataset_a_characteristics_path, dataset_b_characteristics_path]:
        dataset = pandas.read_csv(d_characteristics_path).fillna(0)
        dataset.dex_date = dataset.dex_date.astype('datetime64[s]')
        dataset['year'] = pandas.DatetimeIndex(dataset['dex_date']).year

        if date_fix:
            log.debug("- Fixing date")
            # Get year from elem
            for elem in dataset['sha256']:
                if elem not in date_fix_df['sha256'].values:
                    # log.debugv("Not in date_fix_df, continuing")
                    continue
                # log.debugv("Trying with elem {}".format(elem))
                year = int(dataset.loc[dataset['sha256'] == elem, 'year'].iloc[0])

                # log.debugv("Year for {} is {}".format(elem, year))

                # If year < 2008
                if year < 2008:
                    # if new_year doesn't exists, continue
                    new_year = int(date_fix_df.loc[date_fix_df['sha256'] == elem, 'year_fix'].iloc[0])
                    # log.debugv("- New year is {}".format(new_year))
                    # if new_year is >= 2008
                    if new_year >= 2008:
                        # change year
                        dataset.at[dataset['sha256'] == elem, 'year'] = new_year

        characteristics_year_list.append(dataset[['sha256', 'year']])

    # Load combinations hashes
    log.info("Loading combination hashes")
    dataset_combinations = [ut.load_json_file(dataset_a), ut.load_json_file(dataset_b)]

    # Divide classes by year
    log.info("Dividing by year")
    for dataset_index, dataset in enumerate(dataset_combinations):
        # dataset = dict of classes/list of hashes
        dataset_year = dataset_by_year[dataset_index]
        characteristics_year = characteristics_year_list[dataset_index]

        log.debugv("This df ({}): {}".format(dataset_index, characteristics_year))

        # For class in dataset
        for c in dataset:
            for elem in dataset[c]:
                year = int(characteristics_year.loc[characteristics_year['sha256'] == elem, 'year'].iloc[0])
                if year > 2021:
                    continue
                if c not in dataset_year[year]:
                    dataset_year[year][c] = []
                dataset_year[year][c].append(elem)

    list_training_sets = []
    # Generate training set
    log.info("Generating training set")
    if balance_time_window:
        for year in [y for y in year_list if y <= limit_year]:
            log.info("- Year: {}".format(year))
            first_dataset = dataset_by_year[0][year]
            second_dataset = dataset_by_year[1][year]
            year_training_set = gen_dataset_by_parts(first_dataset, second_dataset, 'f')
            log.debug("  - Size in this year: {}".format(ut.get_size_from_combinations(year_training_set)))
            list_training_sets.append(year_training_set)

        training_set = ut.merge_datasets(list_training_sets)
    else:
        first_list = []
        second_list = []
        for year in [y for y in year_list if y <= limit_year]:
            # log.info("Creating test sets")
            log.info("- Year: {}".format(year))
            first_list.append(dataset_by_year[0][year])
            second_list.append(dataset_by_year[1][year])
            # year_test_set = gen_dataset_by_parts(first_dataset, second_dataset, 'p', percent=percent)
            # list_test_sets.append(year_test_set)

        first_dataset = ut.merge_datasets(first_list)
        second_dataset = ut.merge_datasets(second_list)

        training_set = gen_dataset_by_parts(first_dataset, second_dataset, 'f')

    # Generating test set
    log.info("Generating test set")
    if balance_time_window:
        list_test_sets = []
        for year in [y for y in year_list if y > limit_year]:
            log.info("- Year: {}".format(year))
            first_dataset = dataset_by_year[0][year]
            second_dataset = dataset_by_year[1][year]
            year_test_set = gen_dataset_by_parts(first_dataset, second_dataset, 'p', percent=percent)
            list_test_sets.append(year_test_set)

        test_set = ut.merge_datasets(list_test_sets)
    else:
        first_list = []
        second_list = []
        for year in [y for y in year_list if y > limit_year]:
            log.info("- Year: {}".format(year))
            # log.info("Creating test sets")
            first_list.append(dataset_by_year[0][year])
            second_list.append(dataset_by_year[1][year])
            # year_test_set = gen_dataset_by_parts(first_dataset, second_dataset, 'p', percent=percent)
            # list_test_sets.append(year_test_set)

        first_dataset = ut.merge_datasets(first_list)
        second_dataset = ut.merge_datasets(second_list)

        test_set = gen_dataset_by_parts(first_dataset, second_dataset, 'p', percent=percent)

    sufixes = ['-training', '-test-' + str(percent) + 'p']
    datasets_id = []
    datasets_name = []

    for set, sufix in zip([training_set, test_set], sufixes):
        ds_id = id + sufix
        if not balance_time_window:
            ds_id = ds_id + "_no-bal-time-win"
        datasets_id.append(ds_id)
        output_dir = res_dir + '/' + ds_id
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_json = {
            'name': name + sufix,
            'size': ut.get_size_from_combinations(set),
        }
        datasets_name.append(output_json['name'])

        # Output combinations hashes
        output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.combination_hashes.json'
        ut.dump_json_to_file(set, output_file)
        log.info("Generated dataset combinations_hashes saved as {}".format(output_file))

        # Output characteristics file
        ut.generate_characteristics_file(set, [dataset_a_characteristics_path, dataset_b_characteristics_path], res_dir, ds_id)

        # Output *.classcount.csv file
        output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.classcount.csv'
        combinations_hashes_to_classcount(set, output_dir, ds_id,
                                          output_file, feat_file_name=feat_list_name)
        log.info("Generated dataset classcount saved as {}".format(output_file))

        # Output dataset_class_info file
        output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.dataset_class_info.json'

        ut.dump_json_to_file(output_json, output_file, is_numpy=True)
        log.info("Dataset info saved as {}".format(output_file))

        # log.info("You may want to include this in your config file:\n"
        #          "[{}]\ndataset_name={}\ncamembert_name={}".format(ds_id, output_json['name'], "${dataset_name}"))

    return datasets_id, datasets_name


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate datasets from parts of others")
    parser.add_argument('config_file', help='Full path to the config file')
    parser.add_argument('id', help='New dataset id')
    parser.add_argument('name', help='New dataset full name')
    parser.add_argument('dataset_a', help='First dataset to count')
    parser.add_argument('dataset_b', help='Second dataset to count')
    parser.add_argument('year', type=int, help="Limit year (those below it or in it belongs to the training set, "
                                               "the rest to the test set)")
    parser.add_argument("--percent", type=float, default=5,
                        help="Percent of elements from fist dataset")
    parser.add_argument("--date-fix", help="CSV file with hashes and other dates to take into account")
    parser.add_argument("--no-balance-time-window", action='store_false', help="See C2 Pendelbury")
    parser.add_argument('-v', help='Output information to the standard output (-vv is very verbose)', action="count")

    args = parser.parse_args()

    if args.v is not None:
        verbosity = args.v
        print("Verbosity: " + str(verbosity))
    else:
        verbosity = 0

    logSetup(verbosity)

    if not os.path.exists(args.config_file):
        log.error("{} cannot be found. Exiting".format(args.config_file))
        exit(404)

    config = configparser.ConfigParser(default_section='general', interpolation=configparser.ExtendedInterpolation())
    config.read(args.config_file)

    general_opts = config.defaults()
    sections = config.sections()
    log.debugv("general opts: {}".format(general_opts))
    for elem in ['res_dir', 'visual_output']:
        if elem not in general_opts:
            log.error("{} not specified in config file. Exiting".format(elem))
            exit(11)

    # Input and out directories
    res_dir = os.path.expanduser(config.get('general', 'res_dir'))
    input_dataset_dir = os.path.expanduser(config.get('general', 'input_dataset_dir'))

    # Extract feature file
    feature_file_path = os.path.expanduser(config.get('general', 'selected_features_file'))
    feat_list_name = '.'.join(feature_file_path.split('.')[:-1])

    # ----------------
    # Getting datasets
    # ----------------

    dataset_list = []
    characteristics_path_list = []
    dataset_ids = []
    if args.dataset_a and args.dataset_b:
        arg_list = [args.dataset_a, args.dataset_b]
        for dataset in arg_list:
            if dataset not in sections:
                log.error("\"{}\" dataset not specified in config file section. Exiting".format(dataset))
                exit(11)

        for dataset in arg_list:

            dataset_found = False

            for dataset_dir in [input_dataset_dir, res_dir]:
                combination_hashes_file = os.path.expanduser(dataset_dir + '/' + dataset
                                                             + '/' + dataset + '.' + feat_list_name
                                                             + '.combination_hashes.json')
                if os.path.exists(combination_hashes_file):
                    dataset_list.append(combination_hashes_file)
                else:
                    continue

                characteristics_path = os.path.expanduser(dataset_dir + '/' + dataset
                                                          + '/' + dataset + '.characteristics.csv')
                if os.path.exists(characteristics_path):
                    characteristics_path_list.append(characteristics_path)
                else:
                    continue

                log.info("Using the {} characteristics file found in \"{}\"".format(dataset, characteristics_path))
                dataset_ids.append(dataset)
                dataset_found = True
                break

            if not dataset_found:
                log.error("Can't continue without the {} characteristics or he combination_hashes file."
                          .format(dataset))
                exit(404)

    else:
        log.error("No datasets declared. Exiting")
        exit(404)

    ids, names = gen_datasets_mwgw_year_window(dataset_list[0], dataset_list[1], args.year,
                                               characteristics_path_list[0], characteristics_path_list[1],
                                               args.id, args.name, res_dir, feat_list_name, percent=args.percent,
                                               date_fix=args.date_fix, balance_time_window=args.no_balance_time_window)

    for id, name in zip(ids, names):
        if id not in sections:
            dataset_config_specs = "[{}]\ndataset_name={}\ncamembert_name={}\n\n".format(id, name, "${dataset_name}")
            with open(args.config_file, 'a') as out:
                out.write(dataset_config_specs)

    quit()
