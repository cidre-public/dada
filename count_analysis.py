import argparse
import logging
import os
import pandas
import math
from scipy.stats import chi2, describe
import configparser

import get_class_proportions as gcp
import count_instances as ci
import chi_squared as cs
from utilities import load_json_file, dump_json_to_file

import time
now_str = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def dump_count_analysis_gof(ev_dict, ev_freq, ov_dict, ov_freq, combination_list):
    # Combination population, sample, difference and partial Chi2 result
    combination_psdp = {}
    exp_default_class = 0
    obs_default_class = 0

    for combination in ev_dict:
        # Variable to gather this information:
        # Population proportion, sample proportion, difference and partial Chi2 result
        psdp = {
            'expedcted value': 0,
            'observed_value': 0,
            'residual': 0,
            'population proportion': 0,
            'sample proportion': 0,
            'proportion difference': 0,
            'partial chi2': 0
        }

        # FUTURE: Modify this seciton for homogeneity
        # Count for default observations
        if ev_dict[combination]/ev_freq * ov_freq < 5:
            exp_default_class += ev_dict[combination]
            if combination in ov_dict:
                obs_default_class += ov_dict[combination]
        else:
            p_proportion = ev_dict[combination]/ev_freq
            psdp['expected value'] = p_proportion * ov_freq
            if combination in ov_dict:
                s_proportion = ov_dict[combination]/ov_freq
                psdp['observed value'] = ov_dict[combination]
                # if ov_dict[combination] == 159:
                #     log.debug("Combination index: {}".format(combination_list.index(combination)))
                #     log.debug("Observed value: {}".format(ov_dict[combination]))
                #     log.debug("Expected value: {}".format(p_proportion * ov_freq))
            else:
                s_proportion = 0
                psdp['observed value'] = 0
            diff = p_proportion - s_proportion
            partial_chi2_res = cs.partial_chi2(p_proportion*ov_freq, psdp['observed value'])
            psdp['population proportion'] = p_proportion
            psdp['sample proportion'] = s_proportion
            psdp['proportion difference'] = math.fabs(diff)
            psdp['partial chi2'] = partial_chi2_res
            combination_psdp[combination_list.index(combination)] = psdp

    combination_psdp['default class'] = {}
    default_class = combination_psdp['default class']
    default_class['observed value'] = obs_default_class
    default_class['expected value'] = exp_default_class/ev_freq * ov_freq
    default_class['population proportion'] = exp_default_class/ev_freq
    default_class['sample proportion'] = obs_default_class/ov_freq
    default_class['proportion difference'] = math.fabs(exp_default_class/ev_freq - obs_default_class/ov_freq)
    default_class['partial chi2'] = cs.partial_chi2(exp_default_class/ev_freq*ov_freq, obs_default_class)

    return combination_psdp


def dump_count_analysis_homogeneity(ov_df, ev_df):
    log.debug("Dumping the homogeneity test")
    """
    Specification for the excel sheet (2):
    indexes = combination
    columns = (dataset 1 and 2) observed values, expected values, residual, proportion by dataset total
    """

    # sheet = {}
    d1, d2 = ev_df.index
    cols = [d1 + ' observed values', d2 + ' observed values',
            d1 + ' expected values', d2 + ' expected values',
            d1 + ' residual', d2 + ' residual',
            d1 + ' proportion by dataset total', d2 + ' proportion by dataset total']

    # sheet = pandas.DataFrame(index=ev_df.columns, columns=cols).fillna(0)
    # log.debugv("Empty sheet: \n{}".format(sheet))

    sheet = {}
    for col in cols:
        sheet[col] = {}

    total_dataset = {
        d1: ov_df.sum(1)[d1],
        d2: ov_df.sum(1)[d2]
    }
    sum_square_residuals_less_one = 0
    # obs_val_less_five = 0
    # obs_val_one = 0
    # exp_val_less_five = 0
    # exp_val_less_one = 0
    # num_classes = len(ev_df.columns)

    # Rows (indexes) are the classes (columns in the ev_df)
    indexes = ev_df.columns
    for sheet_index in indexes:
        for dataset in [d1, d2]:
            exp_val = ev_df.at[dataset, sheet_index]
            obs_val = ov_df.at[dataset, sheet_index]
            sheet[dataset + ' observed values'][sheet_index] = obs_val
            sheet[dataset + ' expected values'][sheet_index] = exp_val

            sheet[dataset + ' residual'][sheet_index] \
                = (obs_val - exp_val) \
                / math.sqrt(exp_val)
            sheet[dataset + ' proportion by dataset total'][sheet_index] \
                = obs_val / total_dataset[dataset]

            if exp_val < 1:
                sum_square_residuals_less_one \
                    = sum_square_residuals_less_one \
                    + math.pow(sheet[dataset + ' residual'][sheet_index], 2)

    return sheet, sum_square_residuals_less_one


def get_class_list(list_datasets):
    class_list = []
    for elem in list_datasets:
        for key in elem:
            if key not in class_list:
                class_list.append(key)

    return class_list


def get_delta(rep_dataset, pop_dataset):
    delta = 0
    class_list = get_class_list([rep_dataset, pop_dataset])
    log.debugv("Class list: {}".format(class_list))

    rep_size = sum(rep_dataset.values())
    pop_size = sum(pop_dataset.values())

    for c in class_list:
        if c in rep_dataset:
            rep_proportion = rep_dataset[c] / rep_size
        else:
            rep_proportion = 0
        if c in pop_dataset:
            pop_proportion = pop_dataset[c] / pop_size
        else:
            pop_proportion = 0

        delta += abs(rep_proportion - pop_proportion)

    log.debug("Result delta: {}".format(delta))

    return delta


def get_max_delta(rep_dataset, pop_dataset):
    max_delta = 0
    class_list = get_class_list([rep_dataset, pop_dataset])
    log.debugv("Class list: {}".format(class_list))

    rep_size = sum(rep_dataset.values())
    pop_size = sum(pop_dataset.values())

    for c in class_list:
        if c in rep_dataset:
            rep_proportion = rep_dataset[c] / rep_size
        else:
            rep_proportion = 0
        if c in pop_dataset:
            pop_proportion = pop_dataset[c] / pop_size
        else:
            pop_proportion = 0

        this_delta = abs(rep_proportion - pop_proportion)

        if this_delta > max_delta:
            max_delta = this_delta

    log.debug("Result max_delta: {}".format(max_delta))

    return max_delta


def get_avg_delta(rep_dataset, pop_dataset):
    # avg_delta = 0
    list_delta = []
    class_list = get_class_list([rep_dataset, pop_dataset])
    log.debugv("Class list: {}".format(class_list))

    rep_size = sum(rep_dataset.values())
    pop_size = sum(pop_dataset.values())

    for c in class_list:
        if c in rep_dataset:
            rep_proportion = rep_dataset[c] / rep_size
        else:
            rep_proportion = 0
        if c in pop_dataset:
            pop_proportion = pop_dataset[c] / pop_size
        else:
            pop_proportion = 0

        this_delta = abs(rep_proportion - pop_proportion)

        list_delta.append(this_delta)

        # if this_delta > avg_delta:
        #     avg_delta = this_delta

    avg_delta = sum(list_delta) / len(list_delta)

    log.debug("Result avg_delta: {}".format(avg_delta))

    return avg_delta


def count_analysis(ev_path, population_id, list_ov_path, feature_list_path,
                   datasets_id, datasets_name, res_dir,
                   output_dir=None, test_type='h',
                   xls_name=None, do_description=False, population=True, only_chi2=False):
    """
    Do analysis over count instances: class proportion

    :param ev_path: Full path of the population values (typically a .classcount.csv file)
    values
    :param list_ov_path: Full path of the observed values (typically a .classcount.csv file)
    :param feature_list_path: Full path of the observed values (typically a .classcount.csv file)
    :param output_dir:
    :param xls_name:
    :param population: Tell if the expected values come from a population, in order to calculate the proper expected
    :return: (nothing)

    It outputs the following files:
        - combinations_names_(now time).txt
        - *_density_function.pdf
        - res_count_analysis.xlsx
    """

    if only_chi2:
        log.info("Only performing Chi2 test")

    if output_dir:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

    log.debug("Test type: {}".format(test_type))

    feature_list = []
    if feature_list_path.endswith('.json'):
        # feature_list.append()
        feature_dict = load_json_file(feature_list_path)
        for feat_name in feature_dict:
            feature_list.append(feat_name)
    else:
        feature_list = ci.get_unsorted_selected_feats(feature_list_path)
    log.debug("Feature list:")
    for feat in feature_list:
        log.debug("{}".format(feat))

    # Only get combination_list from "get_counts_combinations"
    combination_list = gcp.get_counts_combinations([ev_path] + list_ov_path)[1]

    # Get the count of expected values
    log.debugv("Getting expected values")
    ev_dict, ev_freq = cs.get_counts(ev_path)
    log.debug("Num of expected values: {}".format(ev_freq))
    log.debug("Num of combinations: {}".format(len(ev_dict.keys())))
    log.debug("Getting observed values")

    # List of DataFrames
    df_list = []

    # List of chi2
    chi2_list = []
    p_value_list = []
    num_combinations_list = []

    # Name of the sheets for the Excel file
    sheet_name_list = []

    # Columns of the Excel sheet per dataset
    chi2_res_dict = {
        'chi2 test result': {},
        'p-value': {},
        'sample size': {},
        'delta': {},
        'max delta': {},
        'avg delta': {},
        'd_min': {},
        'num apps removed': {},
        'num apps added': {},
        'add ratio': {},
        'num classes': {},
        'num categories': {},
        'num classes in common': {},
        'num classes unique to this dataset': {},
        'num classes unique to the reference': {},
        '5% p-value @ num catgs': {},
        'Sum of square of residuals of exp val < 1': {},
        '% classes obs val < 5': {},
        '% classes obs val = 1': {},
        '% classes exp val < 5': {},
        '% classes exp val < 1': {}
    }

    for ov_path, name, dataset_id, num_dataset in zip(list_ov_path, datasets_name, datasets_id, range(len(list_ov_path))):
        log.debug("Calculating chi2 for {}".format(name))
        log.debug("Dataset path: {}".format(ov_path))

        # name = dataset_id
        # Get observed values
        ov_dict, ov_freq = cs.get_counts(ov_path)
        log.debugv("Obtained dict: {}".format(ov_dict))
        num_obs_combinations = len(ov_dict.keys())
        log.debug("Num of combinations: {}".format(num_obs_combinations))
        log.debug("Num of observed values: {}".format(ov_freq))

        if dataset_id == population_id:
            dataset_id = dataset_id + '_self'

        if test_type == 'h':
            log.debug('Doing homogeneity test')
            (cs_res, pvalue), num_categories, ov_df, ev_df \
                = cs.chi2_homogeneity_test([ev_dict, ov_dict], [population_id, dataset_id])
        elif test_type == 'gof':
            (cs_res, pvalue), num_categories \
                = cs.chi2_goodness_of_fit_test(ev_dict, ev_freq, ov_dict, ov_freq, population)
        else:
            log.error("This ({}) test doesn't exits (only in your head probably)".format(test_type))
            exit(44)

        log.info('\nChi-squared test for \'{}\': \n'
                 '- {} (p-value: {}, num categories: {}) Num combinations in sample: {}'
                 .format(name, cs_res, pvalue, num_categories, num_obs_combinations))

        # if dataset_id.endswith('_self') or only_chi2:
        #     continue
        if only_chi2:
            continue

        chi2_list.append(cs_res)
        p_value_list.append(pvalue)
        num_combinations_list.append(num_obs_combinations)
        chi2_res_dict['chi2 test result'][name] = cs_res
        chi2_res_dict['p-value'][name] = pvalue
        chi2_res_dict['sample size'][name] = ov_freq
        chi2_res_dict['num classes'][name] = num_obs_combinations
        chi2_res_dict['num categories'][name] = num_categories
        chi2_res_dict['5% p-value @ num catgs'][name] = chi2.isf(0.05, num_categories - 1)

        if test_type == 'gof':
            combination_psdp = dump_count_analysis_gof(ev_dict, ev_freq, ov_dict, ov_freq,
                                                       combination_list)

            # Formatting psdp to DataFrame for output in CSV
            df = pandas.DataFrame(combination_psdp)
            # log.info("Printing table")
            # print(df)
            # log.info("Printing transposed table")
            tdf = df.T

            log.debug("Calculating total for each column")
            dict_total = {'total': {}}
            for col in tdf.columns:
                total = tdf.sum(axis=0)[col]
                dict_total['total'][col] = total

            # Getting total
            t = pandas.DataFrame(dict_total).T

            # PROTIP: append(sort=False) adds the new rows and keeps the previous column order
            tdf = tdf.append(t, sort=False)
            # print(tdf.sort_values('difference', ascending=False))

            df_list.append(tdf)
            # Strip the '/' in the name (from dataset names)
            sheet_name = name.replace('/', '')
            sheet_name_list.append(sheet_name)
            log.debug('Saving sheet name {}'.format(sheet_name))

            # Dumping Chi-squared tests results in the *.dataset_class_info.json
            filename = res_dir + '/' + dataset_id + '/' + dataset_id + '.dataset_class_info.json'
            dataset_info = load_json_file(filename)
            chi2_data = {
                'population': population_id,
                'chi2 result': cs_res,
                'p-value': pvalue,
                '5% p-value @ num catgs': chi2.isf(0.05, num_categories - 1)
            }
            dataset_info['chi2_test'] = chi2_data
            dump_json_to_file(dataset_info, filename)

        elif test_type == 'h':
            # Combination population, sample, difference and partial Chi2 result
            sheet, sum_square_residuals_lt_one = dump_count_analysis_homogeneity(ov_df, ev_df)

            chi2_res_dict['Sum of square of residuals of exp val < 1'][name] = sum_square_residuals_lt_one

            obs_val_less_five = 0
            obs_val_one = 0

            num_classes = len(ev_df)

            for c_index, value in ov_df.loc[dataset_id].items():
                    if value < 5:
                        obs_val_less_five += 1
                        if value == 1:
                            obs_val_one += 1

            exp_val_less_five = 0
            exp_val_less_one = 0
            for c_index, value in ev_df.loc[dataset_id].items():
                if value < 5:
                    exp_val_less_five += 1
                    if value < 1:
                        exp_val_less_one += 1

            chi2_res_dict['% classes obs val < 5'][name] = obs_val_less_five / num_classes
            chi2_res_dict['% classes obs val = 1'][name] = obs_val_one / num_classes
            chi2_res_dict['% classes exp val < 5'][name] = exp_val_less_five / num_classes
            chi2_res_dict['% classes exp val < 1'][name] = exp_val_less_one / num_classes

            num_classes_uniq_dataset = 0
            num_classes_uniq_population = 0
            num_classes_common = 0

            # Calculating num of classes
            for index, col in ov_df.items():
                if col[dataset_id] != 0:
                    if col[population_id] != 0:
                        num_classes_common += 1
                    else:
                        num_classes_uniq_dataset += 1
                else:
                    if col[population_id] != 0:
                        num_classes_uniq_population += 1

            chi2_res_dict['num classes in common'][name] = num_classes_common
            chi2_res_dict['num classes unique to this dataset'][name] = num_classes_uniq_dataset
            chi2_res_dict['num classes unique to the reference'][name] = num_classes_uniq_population

            # Loading dataset class info
            if dataset_id.endswith('_self'):
                filename = res_dir + '/' + population_id + '/' + population_id + '.' + feat_list_name + '.dataset_class_info.json'
            else:
                filename = res_dir + '/' + dataset_id + '/' + dataset_id + '.' + feat_list_name + '.dataset_class_info.json'

            if os.path.exists(filename):
                dataset_info = load_json_file(filename)
            else:
                dataset_info = {}

            if 'delta' in dataset_info:
                chi2_res_dict['delta'][name] = dataset_info['delta']
            else:
                chi2_res_dict['delta'][name] = None

            chi2_res_dict['max delta'][name] = get_max_delta(ov_dict, ev_dict)
            chi2_res_dict['avg delta'][name] = get_avg_delta(ov_dict, ev_dict)

            if 'd_min final' in dataset_info:
                chi2_res_dict['d_min'][name] = dataset_info['d_min final']
            else:
                chi2_res_dict['d_min'][name] = 0

            if 'added' in dataset_info:
                chi2_res_dict['num apps added'][name] = dataset_info['added']
            else:
                chi2_res_dict['num apps added'][name] = 0

            if 'removed' in dataset_info:
                chi2_res_dict['num apps removed'][name] = dataset_info['removed']
            else:
                chi2_res_dict['num apps removed'][name] = 0

            # if 'modification percent' in dataset_info:
            #     chi2_res_dict['modification percent'][name] = dataset_info['percent modifs']
            # elif 'd_min final' in dataset_info and 'original size' in dataset_info:
            #     chi2_res_dict['modification percent'][name] = dataset_info['d_min final']
            #                                                   / dataset_info['original size']
            # else:
            #     chi2_res_dict['modification percent'][name] = 0

            if 'add ratio' in dataset_info:
                chi2_res_dict['add ratio'][name] = dataset_info['add ratio']
            elif 'added' in dataset_info and 'size' in dataset_info:
                chi2_res_dict['add ratio'][name] = dataset_info['added'] / dataset_info['size']
            else:
                chi2_res_dict['add ratio'][name] = 0

            # Formatting sheet dictionary to DataFrame for output in CSV
            df = pandas.DataFrame(sheet)
            # Add names to tuple indexes (rows)
            df.index.names = feature_list
            log.debug("Indexes of {}: {}".format(name, df.index.names))

            df_list.append(df)
            # Strip the '/' in the name (from dataset names)
            sheet_name = name.replace('/', '')
            sheet_name_list.append(sheet_name)
            log.debug('Saving sheet name {}'.format(sheet_name))

            if dataset_id.endswith('_self'):

                # Dumping Chi-squared tests results in the *.dataset_class_info.json
                chi2_data = {
                    'population': population_id,
                    'chi2 result': cs_res,
                    'p-value': pvalue,
                    '5% p-value @ num catgs': chi2.isf(0.05, num_categories - 1),
                    'Sum of square of residuals of exp val < 1': sum_square_residuals_lt_one
                }
                dataset_info['chi2_test'] = chi2_data
                dump_json_to_file(dataset_info, filename)

    if do_description:
        res_names = ['chi2', 'p_value', 'num classes']
        analysis_desc = {}
        res_lists = [chi2_list, p_value_list, num_combinations_list]
        for res, value_list in zip(res_names, res_lists):
            (min, max), mean, var = describe(value_list)[1:4]
            sd = math.sqrt(var)
            description = {
                'mean': int(mean),
                'variance': int(var),
                'standard deviation': int(sd),
                'max value': int(max),
                'min value': int(min)}
            log.info("\nDescription of the {} results: \n"
                     "  - Mean: {}\n"
                     "  - Variance: {}\n"
                     "  - Standard deviation: {}\n"
                     "  - Max value: {}, Min value: {}".format(res, mean, var, sd, max, min))
            analysis_desc[res] = description

        filename = output_dir + '/' + xls_name + '_description.json'
        dump_json_to_file(analysis_desc, filename)
        log.info("Description file saved as: {}".format(filename))

    # Output the spreadsheets
    if output_dir and not only_chi2:
        if xls_name:
            file_name = output_dir + "/" + xls_name + '.xlsx'
        else:
            file_name = output_dir + "/res_count_analysis.xlsx"
        df_chi2 = pandas.DataFrame(chi2_res_dict)
        df_list = [df_chi2] + df_list
        sheet_name_list = ['chi2 test results'] + sheet_name_list

        with pandas.ExcelWriter(file_name) as writer:
            for sheet, sheet_name in zip(df_list, sheet_name_list):
                if len(sheet_name) >= 31:
                    sheet_name = sheet_name[0:30]
                sheet.to_excel(writer, sheet_name=sheet_name, merge_cells=False)

        log.info("Files saved as: {}".format(file_name))

        latex_string = df_chi2[['max delta', 'sample size']].to_latex()

        latex_file = output_dir + "/" + xls_name + '.tex'
        with open(latex_file, 'w') as lf:
            lf.write(latex_string)

        log.info("File saved as: {}".format(latex_file))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Wrapper function for calculate chi-squared + class proportions in "
                                                 "count intances")
    parser.add_argument('config_file', help='Full path to the config file')
    parser.add_argument('--population', required=True, help='Name of the reference dataset')
    parser.add_argument('-t', '--test-type', default='h', help='Test type (gof for goodness of fit, h for homogeneity) '
                                                               'default = h')
    parser.add_argument('--datasets', required=True, nargs='+', help='List of names of the datasets')
    parser.add_argument('--description', action='store_true',
                        help='Perform mean, variance, standard deviation, min and max value calculations '
                             'for the list of Chi2 test and p-value results')
    parser.add_argument('--only-chi2', action='store_true',
                        help='Perform only the Chi2 test, don\'t save any result')
    parser.add_argument('--append-population-name', action='store_true',
                        help='Add population name as part of results file')
    parser.add_argument('-f', '--feature-file', help='Path to the selected features files (this overwrites the one in'
                                                     'the config file)')
    parser.add_argument('-n', '--filename', default='res_count_analysis',
                        help='Name of output result Excel file')
    parser.add_argument('--name-by-feature-file', action='store_true',
                        help='Add feature file name as part of results')
    parser.add_argument('-d', '--add-date', action='store_true',
                        help='Add the date as part of the result file\'s name')
    parser.add_argument('-v', help='Output debug information to the standard output (-vv is very verbose)',
                        action="count")

    args = parser.parse_args()

    if args.v is not None:
        verbosity = args.v
        print("Verbosity: " + str(verbosity))
    else:
        verbosity = 0

    logSetup(verbosity)

    if not os.path.exists(args.config_file):
        log.error("{} cannot be found. Exiting".format(args.config_file))
        exit(404)

    config = configparser.ConfigParser(default_section='general', interpolation=configparser.ExtendedInterpolation())
    config.read(args.config_file)

    general_opts = config.defaults()
    for elem in ['res_dir', 'selected_features_file', 'count_analysis_output']:
        if elem not in general_opts:
            log.error("{} not specified in config file. Exiting".format(elem))
            exit(11)

    # Input and output dirs
    res_dir = os.path.expanduser(config.get('general', 'res_dir'))
    input_dataset_dir = os.path.expanduser(config.get('general', 'input_dataset_dir'))

    log.debug("Results dir: {}".format(res_dir))

    # Extract feature file
    feature_list_path = ""
    if args.feature_file:
        if os.path.exists(args.feature_file):
            feature_list_path = os.path.expanduser(args.feature_file)
    else:
        feature_list_path = os.path.expanduser(config.get('general', 'selected_features_file'))
    log.info("Feature list path: {}".format(feature_list_path))
    feat_list_name = '.'.join(feature_list_path.split('.')[:-1])

    count_analysis_output = os.path.expanduser(config.get('general', 'count_analysis_output'))
    log.debug("Count analysis path: {}".format(count_analysis_output))

    res_file_name = args.filename
    if args.append_population_name:
        res_file_name = res_file_name + '_' + args.population
    if args.name_by_feature_file:
        res_file_name = res_file_name + '_' + feat_list_name
    if args.add_date:
        log.debug("Adding date to Excel file: {}".format(now_str))
        res_file_name = res_file_name + '_' + now_str
    log.debug("Result file name: {}".format(res_file_name))

    # ----------------------
    # Get population dataset
    # ----------------------

    dataset = args.population
    if dataset not in config.sections():
        log.error("Dataset \"{}\" doesn't exists in the config file. Exiting".format(dataset))
        exit(13)

    population_path = input_dataset_dir + '/' + dataset + '/' + dataset + '.' + feat_list_name + '.classcount.csv'

    if not os.path.exists(population_path):
        log.error("Classcount for population \"{}\" not found in \"{}\". Exiting".format(dataset, population_path))
        exit(14)

    # --------------------
    # Get samples datasets
    # --------------------

    samples_path = []
    datasets_name = []
    datasets_id = []
    for dataset in args.datasets:
        if dataset not in config.sections():
            log.warning("Dataset \"{}\" doesn't exists in the config file. Not adding it".format(dataset))
            continue

        dataset_found = False

        for dataset_dir in [input_dataset_dir, res_dir]:
            classcount_path = dataset_dir + '/' + dataset + '/' + dataset + '.' + feat_list_name + '.classcount.csv'

            if os.path.exists(classcount_path):
                samples_path.append(classcount_path)
            else:
                continue

            dataset_name = config.get(dataset, 'dataset_name', vars=None)

            if not dataset_name:
                log.error("No dataset_name specified for \"{}\". Cannot continue".format(dataset))
                exit(12)

            log.info("Using the {} classcount file found in \"{}\"".format(dataset, classcount_path))

            datasets_id.append(dataset)
            datasets_name.append(dataset_name)
            dataset_found = True
            break

        if not dataset_found:
            log.warning("Classcount file for \"{}\" doesn't exists. Not adding it".format(dataset))
            continue

    # if

    for dataset, name in zip([population_path] + samples_path, ['population'] + datasets_name):
        log.debug("Dataset '{}' has name '{}'".format(dataset, name))

    count_analysis(population_path, args.population, samples_path, feature_list_path,
                   datasets_id, datasets_name, res_dir,
                   output_dir=count_analysis_output, test_type=args.test_type,
                   xls_name=res_file_name, do_description=args.description,
                   only_chi2=args.only_chi2)

    quit()
