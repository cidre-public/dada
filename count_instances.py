import logging
import json
import csv
import time
import datetime
import os
from utilities import dump_json_to_file


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def get_sorted_selected_feats(feature_list_path):
    with open(feature_list_path) as f:
        selected_feats = f.readline().strip().split(',')
    return tuple(sorted(list(set(selected_feats))))


def get_unsorted_selected_feats(feature_list_path):
    selected_feats = []
    with open(feature_list_path) as f:
       # selected_feats = f.readline().strip().split(',')
        for line in f:
            selected_feats.append(line.strip())
    return selected_feats


# FUTURE WORK: Test hashes from file
test_hash_list = ['0C517093BB01FAEFA99C1E03E24313C4FE7E8AEEFC62D63C0526EA4A88577A00',
                  'e36e6c0cff24e41f82faa869e5b56d01',
                  'AADADDE1A730F4A6750AFA32C52C4147A33046AF703E8C9142FDFE3BAECAE6AB',  # no permissions
                  'DDACFB2B9E0D465EFB914E935A5B5029DCD31DFB580113F7009B88E39D6BDAF1',  # read phone state, write external storage
                  'AADADDE1A730F4A6750AFA32C52C4147A33046AF703E8C9142FDFE3BAECAE6AB',  # no permissions
                  'C6F2D6BF7435114F70C312F1441D778E32636E2E5115C0DDE546F89F78ADD43D'  # internet
                  ]


def combinations_hashes_to_classcount(dataset, output_dir, output_name, filename, feat_file_name=None):
    to_output = []
    for combination in dataset:
        to_output.append([len(dataset[combination])] + combination.split(','))

    # if feat_file_name:
    #     filename = output_dir + '/' + output_name + '.' + feat_file_name + '.classcount.csv'
    # else:
    #     filename = output_dir + '/' + output_name + '.classcount.csv'

    log.debug("- Saving *.classcount.csv in {}".format(filename))

    with open(filename, 'w') as out:
        for row in to_output:
            writer = csv.writer(out)
            writer.writerow(row)


def count_instances_csv(feature_list_path, res_dir, list_dataset_path, datasets_name, datasets_id, overwrite=False):
    """
    Count the number observations of different classes in a dataset.
    A class is a unique combination of features
    :param feature_list_path:
    :param output_dir:
    :param list_dataset_path:
    :param suffix:
    :return: (nothing)

    It outputs the following files:
        - *.dataset_class_info.json
        - *.classcount.csv
        - *.combination_hashes.json
    """

    if not os.path.exists(res_dir):
        os.makedirs(res_dir)

    t_start = time.time()
    t_previous = t_start

    # Load the selected_features to take into account
    # feature_list = get_sorted_selected_feats(feature_list_path)
    raw_feature_list = get_unsorted_selected_feats(feature_list_path)
    log.debugv("Raw feature list size: {}".format(len(raw_feature_list)))

    """
    dict_feature_param = {
        "feat 1":{
            "type": *either int or date*
            "value": *column name*
        }   
    }
    """
    dict_feature_param = {}
    feature_list = []
    for feat in raw_feature_list:
        # log.debug("Viewing feat {}".format(feat))
        # input("Press the any key to continue...")
        feat_l = feat.split('.')
        if len(feat_l) > 2:
            typ = feat_l[-2]
        else:
            typ = ''
        if typ == "@i" or typ == "@d":
            feat_name = '.'.join(feat_l[:-2])
            dict_feature_param[feat_name] = {}
            this_feat = dict_feature_param[feat_name]
            this_feat["type"] = typ
            this_feat["value"] = feat_l[-1]
            feature_list.append(feat_name)
        else:
            feature_list.append(feat)

    log.debug("Feature list:")
    for feat in feature_list:
        log.debug("{}".format(feat))

    log.debugv('The new feature list: ' + str(feature_list)
               + ' of length ' + str(len(feature_list)))

    # input("Press the any key to continue...")

    """
    Variables explanation:
    
    feature_list: List of the selected features
    feature_index_in_dataset: Index number of the selected features in the dataset
    feature_val_list: Feature value for a unit
    """

    dict_dataset_combinations = {}

    # Load the dataset's feature matrix
    for dataset_path, name, this_dataset_id in zip(list_dataset_path, datasets_name, datasets_id):
        # output_name = dataset_path.split("/")[-1].split('.')[0]
        output_name = this_dataset_id
        output_dir = res_dir + '/' + this_dataset_id

        if os.path.exists(output_dir + '/' + this_dataset_id + '.dataset_class_info.json') and not overwrite:
            log.warning("The dataset_class_info file is here. I'm afraid I can't continue")
            continue

        dataset_class_info = {
            'name': name,
            'modified': False,
            'size': 0,
            'number features': len(feature_list),
            'number combinations': 0
        }

        with open(dataset_path) as dataset_entry:
            log.info("Counting " + output_name)
            log.debugv("Processing file {}".format(dataset_path))

            # Get the dataset matrix header (the selected_features in the matrix)
            dataset_features = dataset_entry.readline().strip().split(',')
            log.debugv("dataset_features has {} entries".format(len(dataset_features)))

            # Get the index of the selected features in the dataset's feature list
            feature_index_in_dataset = {feat: index for index, feat in enumerate(dataset_features)
                                        if feat in feature_list}

            for feat in feature_index_in_dataset:
                log.debugv('Feature {} has index {} in the feature_index_in_dataset list'
                           .format(feat, feature_index_in_dataset[feat]))

            # Dictionary containing the 'selected selected_features', and the count data
            dataset_feature_list = {
                'selected_features': feature_list,
                'data': {}
            }
            data = dataset_feature_list['data']

            # Dictionary of combinations, where for each combination key,
            # its value is the number of times it appears in the dataset
            combination_count = {}

            # Dictionary structure for the *.combinations_hashes.json file
            hash_list_by_combination = {}
            entry_num = 0

            # For each unit in the dataset
            for line in dataset_entry:

                unit_feature_list = []

                feature_val_list = line.strip().split(',')
                this_hash = feature_val_list[0]
                if this_hash in test_hash_list:
                    log.debug("Looking at " + this_hash)
                    log.debug("Number of feature values: {}".format(len(feature_val_list)))

                # For each selected feature:
                for feat in feature_list:
                    # If feature in unit:
                    if feat in feature_index_in_dataset:
                        # Get the index of the feature value
                        index = feature_index_in_dataset[feat]
                        if this_hash in test_hash_list:
                            log.debug('Feature {} with index {} has a value of {}'
                                      .format(dataset_features[index], index, feature_val_list[index]))

                        value = feature_val_list[index]
                        # log.debugv("{} for feature {} at index {} has value {}".format(this_hash, feat, index, value))

                        if feat in dict_feature_param:
                            if dict_feature_param[feat]['type'] == "@i":
                                value = str(int(int(value) / int(dict_feature_param[feat]['value'])))
                                # print("hello")
                            elif dict_feature_param[feat]['type'] == "@d":
                                # value = int(int(value) / dict_feature_param[feat]['value'])
                                # value = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(1347517370))
                                value = time.strftime(dict_feature_param[feat]['value'], time.localtime(int(value)))

                        # Mark feature as seen in the unit feature value list
                        # Add the feature value of this unit to the list
                        unit_feature_list.append(value)
                    else:
                        unit_feature_list.append(0)

                # Add the hash and result to dictionary
                data[this_hash] = ','.join(unit_feature_list)

                # Count the combination
                combination = tuple(unit_feature_list)
                if combination in combination_count:
                    combination_count[combination] += 1
                else:
                    combination_count[combination] = 1

                # combination_name = ""
                # for feat_num, feat in enumerate(combination):
                #     # if feat != '0':
                #     combination_name += str(feature_list[feat_num]) + ","
                # combination_name = combination_name[:-1]

                combination_str = ','.join(combination)
                if combination_str not in hash_list_by_combination:
                    hash_list_by_combination[combination_str] = {}
                    # hash_list_by_combination[combination_str]['name'] = combination_name
                    hash_list_by_combination[combination_str]['list'] = []

                hash_list_by_combination[combination_str]['list'].append(this_hash)

                # Output a progress every 10 000 reads
                entry_num += 1
                if entry_num % 10000 == 0:
                    t_now = time.time()
                    elapsed_time = str(datetime.timedelta(seconds=t_now - t_start))
                    diff_prev = str(datetime.timedelta(seconds=t_now - t_previous))
                    log.info("{} entries seen so far (Total elapsed time: {}, difference with previous: {})"
                             .format(entry_num, elapsed_time, diff_prev))
                    t_previous = t_now

            log.info("Finished with {}".format(output_name))

            dataset_class_info['size'] = entry_num
            dataset_class_info['number combinations'] = len(combination_count)
            dataset_class_info['combinations hashes'] = hash_list_by_combination

        # Output the counts (as .classcount.csv)
        to_output = []
        for key in combination_count:
            to_output.append([combination_count[key]] + list(key))

        filename = output_dir + '/' + output_name + '.dataset_class_info.json'
        dump_json_to_file(dataset_class_info, filename)
        log.info("File saved as {}".format(filename))

        # Save the .classcount.csv file
        filename = output_dir + '/' + output_name + '.classcount.csv'
        with open(filename, 'w') as out:
            for row in to_output:
                writer = csv.writer(out)
                writer.writerow(row)

        log.info("File saved as {}".format(filename))

        # Output the individual list (for re-creation)
        # This file contains the (sorted) feature list
        # + the data as a dictionary of file hash (according to the dataset output)
        filename = output_dir + '/' + 'count_data_' + output_name + '.json'
        with open(filename, 'w') as out:
            json.dump(dataset_feature_list, out)
        log.info("File saved as {}".format(filename))

        filename = output_dir + '/' + output_name + '.combination_hashes.json'
        with open(filename, 'w') as out:
            json.dump(hash_list_by_combination, out)
        log.info("File saved as {}".format(filename))

        t_end = time.time()
        total_elapsed_time = str(datetime.timedelta(seconds=t_end - t_start))
        log.info('Time elapsed: {}'.format(total_elapsed_time))

        dict_dataset_combinations[name] = hash_list_by_combination

    log.info('Files saved')
    t_end = time.time()
    total_elapsed_time = str(datetime.timedelta(seconds=t_end - t_start))
    log.info('Total time elapsed: {}'.format(total_elapsed_time))

    return dict_dataset_combinations

