# Table III - Drebin_debiased + NAZE_debiased 10% malware
python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "DR-AG-C2_deb" "DR-AG-C2_deb" \
    Drebin_deb-01 \
    NAZE-18-G_deb-001 \
    2013 \
    --percent 10 \
    --no-balance-time-window

# with the C2 option

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "DR-AG-C2_deb" "DR-AG-C2_deb" \
    Drebin_deb-01 \
    NAZE-18-G_deb-001 \
    2013 \
    --percent 10


# Table III - VS_debiased 15-18 + NAZE_debiased 10% malware
python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "VS-AG_deb" "VS-AG_deb" \
    VS15-18_deb-01 \
    NAZE-18-G_deb-001 \
    2016 \
    --percent 10 \
    --no-balance-time-window

# with the C2 option

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "VS-AG-C2_deb" "VS-AG-C2_deb" \
    VS15-18_deb-01 \
    NAZE-18-G_deb-001 \
    2016 \
    --percent 10


# Table III - VS_debiased 15-18 delta = 0.04 + NAZE_debiased 10% malware

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "VS-AG_deb-04" "VS-AG_deb-04" \
    VS15-18_deb-04 \
    NAZE-18-G_deb-01 \
    2016 \
    --percent 10 \
    --no-balance-time-window


# Table III - Drebin + AZ19_100k-G

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "DR-AG" "DR-AG" \
    Drebin \
    AZ19_100k-G \
    2013 \
    --percent 10 \
    --no-balance-time-window


# Table III - VS15-18 + AZ19_100k-G

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "VS-AG" "VS-AG" \
    VS_15-18 \
    AZ19_100k-G \
    2016 \
    --percent 10 \
    --no-balance-time-window


# Table III - AndroCT

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "ACT14" "ACT14" \
    ACT-M \
    ACT-G \
    2013 \
    --percent 10 \
    --no-balance-time-window \
    --date-fix androct.year_fix.sha256.csv

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "ACT17" "ACT17" \
    ACT-M \
    ACT-G \
    2016 \
    --percent 10 \
    --no-balance-time-window \
    --date-fix androct.year_fix.sha256.csv


# Table III - AZ20_30k with labels

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "AZL14" "AZL14" \
    AZL-M \
    AZL-G \
    2013 \
    --percent 10 \
    --no-balance-time-window

python3 gen_dataset_by_parts_subset_year_c.py config.datasets.ini \
    "AZL17" "AZL7" \
    AZL-M \
    AZL-G \
    2016 \
    --percent 10 \
    --no-balance-time-window
