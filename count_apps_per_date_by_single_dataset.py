import argparse
import configparser
import json
import logging
import os
from tabulate import tabulate

import pandas


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def count_apps_per_date_by_single_dataset(source_dataset_path_list, source_dataset_ids, source_dataset_names,
                                          output_dir, dataset_path_list=None, dataset_id_list=None,
                                          dataset_name_list=None, date_fix=None):
    """

    """

    dataset_hashes = []
    datasets_dict = {}
    if dataset_path_list:
        log.debug("- Using references")
        for ref_dataset_path, dataset_id in zip(dataset_path_list, dataset_id_list):
            dataset = pandas.read_csv(ref_dataset_path).fillna(0)
            dataset.dex_date = dataset.dex_date.astype('datetime64[s]')
            dataset['year'] = pandas.DatetimeIndex(dataset['dex_date']).year

            dataset_hashes = dataset_hashes + list(dataset['sha256'])
            datasets_dict[dataset_id] = dataset
            # log.debug("Using this dataset: \n{}".format(dataset))

    dataset_hashes = list(set(dataset_hashes))

    if date_fix:
        log.debug("- Using date_fix")
        date_fix_df = pandas.read_csv(date_fix).fillna(0)

    # Num of elements per source dataset by year
    year_dataset_dict = {}

    # Num of elements in total per source dataset
    source_total_count = {}

    for source_dataset_file_path, source_dataset_id, source_name \
            in zip(source_dataset_path_list, source_dataset_ids, source_dataset_names):

        log.debug("Source: {}".format(source_name))

        source_dataset_df = pandas.read_csv(source_dataset_file_path).fillna(0)
        source_dataset_df.dex_date = source_dataset_df.dex_date.astype('datetime64[s]')
        source_dataset_df['year'] = pandas.DatetimeIndex(source_dataset_df['dex_date']).year

        if dataset_hashes:
            source_dataset_df = source_dataset_df[source_dataset_df['sha256'].isin(dataset_hashes)]

        if date_fix:
            log.debug("- Fixing date")

            # Get year from elem
            for elem in source_dataset_df['sha256']:
                if elem not in date_fix_df['sha256'].values:
                    # log.debugv("Not in date_fix_df, continuing")
                    continue
                # log.debugv("Trying with elem {}".format(elem))
                year = int(source_dataset_df.loc[source_dataset_df['sha256'] == elem, 'year'].iloc[0])
                # log.debugv("Year for {} is {}".format(elem, year))

                if year < 2008:
                    # if new_year doesn't exists, continue
                    new_year = int(date_fix_df.loc[date_fix_df['sha256'] == elem, 'year_fix'].iloc[0])
                    # log.debugv("- New year is {}".format(new_year))
                    if new_year >= 2008:
                        # change year
                        source_dataset_df.at[source_dataset_df['sha256'] == elem, 'year'] = new_year

        for dataset_id, dataset_name in zip(datasets_dict, dataset_name_list):
            log.debug("- Dataset: {}".format(dataset_name))
            dataset = datasets_dict[dataset_id]

            dataset_df = source_dataset_df[source_dataset_df['sha256'].isin(list(dataset['sha256']))]
            log.debug("  - Num hashes of dataset in source: {}".format(len(dataset_df.index)))

            counts = dataset_df['year'].value_counts().sort_index().to_dict()

            for year in counts:
                log.debug("  - year: {}".format(year))
                # Counting by year, source, and dataset
                if year not in year_dataset_dict:
                    year_dataset_dict[year] = {}

                dataset_year = year_dataset_dict[year]

                if source_name not in dataset_year:
                    dataset_year[source_name] = {}

                dataset_counts = dataset_year[source_name]

                dataset_counts[dataset_name] = counts[year]
                log.debug("    - Count: {}".format(counts[year]))

                # Counting source and dataset total
                if source_name not in source_total_count:
                    source_total_count[source_name] = {}

                source_count_total = source_total_count[source_name]

                if dataset_name not in source_count_total:
                    source_count_total[dataset_name] = 0

                source_count_total[dataset_name] += counts[year]

    # Counting tabular year by year

    cut_range = 25
    list_years = sorted(list(year_dataset_dict.keys()))
    for year in list_years:
        dataset_year = year_dataset_dict[year]

        print("Total for {}: ".format(year))

        df = pandas.DataFrame(dataset_year).fillna(0)

        columns = df.columns
        new_names = {}

        for col in columns:
            new_names[col] = "\n".join([col[i:i + cut_range] for i in range(0, len(col), cut_range)])

        # log.debug("new names: {}".format(new_names))

        df = df.rename(columns=new_names)
        df['Total'] = df[list(df.columns)].sum(axis=1)
        log.debug("df: \n{}".format(df))

        print(tabulate(
            df,
            headers='keys',
            stralign='left',
            tablefmt="psql"
        ))

        print()

    # Counting tabular total

    df = pandas.DataFrame(source_total_count).fillna(0)

    if len(df.values) == 0:
        log.warning("No elements in commun in between the datasets and the source datasets")
    else:
        print("Total: ")
        columns = df.columns
        new_names = {}

        for col in columns:
            new_names[col] = "\n".join([col[i:i+cut_range] for i in range(0, len(col), cut_range)])

        df = df.rename(columns=new_names)
        df['Total'] = df[list(df.columns)].sum(axis=1)

        print(tabulate(
            df,
            headers='keys',
            stralign='left',
            tablefmt="psql"
        ))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Count number of hashes in datasets from source datasets")
    parser.add_argument('config_file', help='Full path to the config file')
    parser.add_argument('--datasets', nargs='+', help="List of datasets to count")
    parser.add_argument('--source-datasets', nargs='+', help="List of source datasets")
    parser.add_argument("--date-fix", help="CSV file with hashes and other dates to take into account")
    parser.add_argument('-v', help='Output information to the standard output (-vv is very verbose)', action="count")

    args = parser.parse_args()

    if args.v is not None:
        verbosity = args.v
        print("Verbosity: " + str(verbosity))
    else:
        verbosity = 0

    logSetup(verbosity)

    if not os.path.exists(args.config_file):
        log.error("{} cannot be found. Exiting".format(args.config_file))
        exit(404)

    config = configparser.ConfigParser(default_section='general', interpolation=configparser.ExtendedInterpolation())
    config.read(args.config_file)

    general_opts = config.defaults()
    sections = config.sections()
    log.debugv("general opts: {}".format(general_opts))
    for elem in ['res_dir', 'visual_output']:
        if elem not in general_opts:
            log.error("{} not specified in config file. Exiting".format(elem))
            exit(11)

    # Input and out directories
    res_dir = os.path.expanduser(config.get('general', 'res_dir'))
    input_dataset_dir = os.path.expanduser(config.get('general', 'input_dataset_dir'))

    visual_output_dir = os.path.expanduser(config.get('general', 'visual_output'))

    # Extract feature file
    feature_file_path = os.path.expanduser(config.get('general', 'selected_features_file'))
    feat_list_name = '.'.join(feature_file_path.split('.')[:-1])

    source_dataset_list = []
    source_dataset_names = []
    source_characteristics_path_list = []
    source_dataset_ids = []

    if args.source_datasets:
        # arg_list = [args.dataset_a, args.dataset_b]
        for dataset in args.source_datasets:
            if dataset not in sections:
                log.error("Dataset \"{}\" not specified in config file section. Exiting".format(dataset))
                exit(11)

        for dataset in args.source_datasets:

            dataset_found = False

            for dataset_dir in [input_dataset_dir, res_dir]:
                combination_hashes_file = os.path.expanduser(dataset_dir + '/' + dataset
                                                             + '/' + dataset + '.' + feat_list_name
                                                             + '.combination_hashes.json')
                if os.path.exists(combination_hashes_file):
                    source_dataset_list.append(combination_hashes_file)
                else:
                    continue

                characteristics_path = os.path.expanduser(dataset_dir + '/' + dataset
                                                          + '/' + dataset + '.characteristics.csv')
                if os.path.exists(characteristics_path):
                    source_characteristics_path_list.append(characteristics_path)
                else:
                    continue

                log.info("Using the {} combination hashes file found in \"{}\"".format(dataset, combination_hashes_file))
                log.info("Using the {} characteristics file found in \"{}\"".format(dataset, characteristics_path))
                source_dataset_ids.append(dataset)
                source_dataset_names.append(config.get(dataset, 'dataset_name'))
                dataset_found = True
                break

            if not dataset_found:
                log.error("Cannot continue without the {} characteristics or the combinations_hashes file."
                          .format(dataset))
                exit(404)
            log.debug("Using {} as source dataset".format(dataset))
    else:
        log.error("No datasets declared. Exiting")
        exit(404)

    datasets_characteristics_paths = []
    datasets_ids = []
    datasets_names = []
    if args.datasets:
        for dataset in args.datasets:
            if dataset not in sections:
                log.error("Dataset \"{}\" not specified in config file section. Exiting".format(dataset))
                exit(11)

        for dataset in args.datasets:

            dataset_found = False

            for dataset_dir in [input_dataset_dir, res_dir]:

                characteristics_path = os.path.expanduser(dataset_dir + '/' + dataset
                                                          + '/' + dataset + '.characteristics.csv')
                if os.path.exists(characteristics_path):
                    datasets_characteristics_paths.append(characteristics_path)
                else:
                    log.warning("Can't continue with the {} characteristics file ({} not found). Searching elsewhere."
                                .format(dataset, characteristics_path))
                    continue

                datasets_ids.append(dataset)
                datasets_names.append(config.get(dataset, 'dataset_name', vars=None))
                dataset_found = True
                break

            if not dataset_found:
                log.error("Characteristics file for reference dataset {} doesn't exists anywhere".format(dataset))
                exit(404)

            log.info("Using the {} characteristics file found in \"{}\"".format(dataset, characteristics_path))

    count_apps_per_date_by_single_dataset(source_characteristics_path_list, source_dataset_ids, source_dataset_names,
                                          visual_output_dir,
                                          datasets_characteristics_paths, datasets_ids, datasets_names, args.date_fix)

    quit()
