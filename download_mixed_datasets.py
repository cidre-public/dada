import sys
import os.path
from os import path
from get_apk_from_androzoo import get_apk_from_androzoo
from get_apk_from_virusshare import get_apk_from_virusshare

listdataset = [
    ("DR-AG_deb", "DR-AG_deb"),
    ("DR-AG-C2_deb", "DR-AG-C2_deb"),
    ("VS-AG_deb", "VS-AG_deb"),
    ("VS-AG-C2_deb", "VS-AG-C2_deb"),
    ("VS-AG_deb-04", "VS-AG_deb-04"),
    ("DR-AG", "DR-AG"),
    ("VS-AG", "VS-AG"),
    ("ACT14", "ACT14"),
    ("ACT17", "ACT17"),
    ("AZL14", "AZL14"),
    ("AZL17", "AZL17"),
]

if len(sys.argv) <= 3:
    print(
        "Usage: python3 download_mixed_datasets.py api_key_androzoo api_key_virusshare outdir"
    )
    quit()

api_key_androzoo = sys.argv[1]
api_key_virusshare = sys.argv[2]
outdir = sys.argv[3]

for (dataset, datasetfile) in listdataset:
    print("Dataset: " + dataset)
    print("Files are prefixed: " + datasetfile)

    for part in ["training", "test"]:
        print(
            "=========================================================================="
        )
        sha256file = "datasets/" + datasetfile + "-" + part + ".sha256.txt"

        datasetdownloadfolder = outdir + "/" + dataset + "/" + datasetfile + "-" + part

        if not path.exists(sha256file):
            print("ERROR: missing file " + sha256file)
            exit()

        if not path.exists(datasetdownloadfolder):
            print("Creating directory " + datasetdownloadfolder)
            os.makedirs(datasetdownloadfolder)

        print("Downloading " + sha256file)
        hashes_not_found_androzoo = get_apk_from_androzoo(
            api_key_androzoo, sha256file, datasetdownloadfolder
        )

        print(datasetdownloadfolder)

        downloaded_files = [
            name
            for name in os.listdir(datasetdownloadfolder)
            if os.path.isfile(datasetdownloadfolder + "/" + name)
        ]

        print("Counting: " + str(len(downloaded_files)))

        if path.exists(hashes_not_found_androzoo):
            print("Trying to download from VirusShare")

            get_apk_from_virusshare(
                api_key_virusshare, hashes_not_found_androzoo, datasetdownloadfolder
            )
