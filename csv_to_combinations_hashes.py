import argparse
import configparser
import csv
import json
import logging
import os
import shutil

from utilities import dump_json_to_file


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def csv_to_combination_hashes(datasets_id, csv_list, feat_file_name, feat_file_path, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    if os.path.exists(feat_file_path):
        try:
            log.debug("Loading {}".format(feature_file_path))
            with open(feature_file_path) as f:
                raw_features = json.load(f)
        except ValueError as error:
            log.error("Cannot convert JSON: {}".format(error))
            exit(12)

    num_feats = len(raw_features.keys())

    start_feat = 1

    for d_id, d_csv_path in zip(datasets_id, csv_list):
        log.info("Generating combination_hashes file for \"{}\"".format(d_id))
        combination_hashes = {}
        combination_count = {}
        dataset_dir = output_dir + '/' + d_id

        os.makedirs(dataset_dir, exist_ok=True)

        shutil.copy2(d_csv_path, dataset_dir)

        ch_output_path = os.path.join(dataset_dir, d_id + '.' + feat_file_name + '.combination_hashes.json')

        if os.path.exists(ch_output_path):
            log.info("- Combinations hashes for \"{}\" already exists, continuing".format(d_id))
            continue

        with open(d_csv_path) as f:
            headers = f.readline().strip().split(',')[start_feat:num_feats+start_feat]

            log.debug("Headers for \"{}\": {}".format(d_id, headers))
            # input("hold")
            for line in f:
                line_split = line.strip().split(',')
                app_hash = line_split[0]
                app_class = ','.join(line_split[start_feat:num_feats+start_feat])

                # log.debug("This app \"{}\" of size \"{}\" is class: {}"
                #           .format(app_hash, line_split[3], app_class))

                if app_class not in combination_hashes:
                    combination_hashes[app_class] = []

                if app_class not in combination_count:
                    combination_count[app_class] = 0

                combination_hashes[app_class].append(app_hash)
                combination_count[app_class] += 1

        dump_json_to_file(combination_hashes, ch_output_path)

        log.info("Combination hashes saved as \"{}\"".format(ch_output_path))

        # Save the .classcount.csv file
        to_output = []
        for key in combination_count:
            to_output.append([combination_count[key]] + key.split(','))

        # log.debug("combination_count: \n{}".format(json.dumps(to_output, indent=4)))

        classcount_filename = os.path.join(dataset_dir, d_id + '.' + feat_file_name + '.classcount.csv')

        with open(classcount_filename, 'w') as out:
            for row in to_output:
                writer = csv.writer(out)
                writer.writerow(row)

        log.info("Classcount saved as \"{}\"".format(classcount_filename))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create combination hashes files from characteristics CSV files")
    parser.add_argument('config_file', help='Full path to the config file')
    parser.add_argument('csv_dir', help='The path to the *.characteristics.csv files')
    parser.add_argument('--ids', nargs='+', help='List of ids of the datasets to convert')
    parser.add_argument('-v', help='Output information to the standard output (-vv is very verbose)', action="count")

    args = parser.parse_args()

    if args.v is not None:
        verbosity = args.v
        print("Verbosity: " + str(verbosity))
    else:
        verbosity = 0

    logSetup(verbosity)

    if not os.path.exists(args.config_file):
        log.error("{} cannot be found. Exiting".format(args.config_file))
        exit(404)

    if not os.path.exists('config.datasets.ini'):
        shutil.copy2(args.config_file, 'config.datasets.ini')

    config_file = 'config.datasets.ini'

    config = configparser.ConfigParser(default_section='general', interpolation=configparser.ExtendedInterpolation())
    config.read(config_file)

    general_opts = config.defaults()
    log.debugv("general opts: {}".format(general_opts))
    for elem in ['res_dir']:
        if elem not in general_opts:
            log.error("{} not specified in config file. Exiting".format(elem))
            exit(11)

    sections = config.sections()
    res_dir = os.path.expanduser(config.get('general', 'input_dataset_dir'))

    feature_file_path = os.path.expanduser(config.get('general', 'selected_features_file'))

    if feature_file_path:
        feat_list_name = '.'.join(feature_file_path.split('.')[:-1])
    else:
        log.error("No feature file path found, exiting")
        exit()

    output_dir = os.path.expanduser(res_dir)

    csv_dir = os.path.expanduser(args.csv_dir)

    if not os.path.exists(csv_dir):
        log.error("The characteristics dir (csv_dir) doesn't exists, exiting")
        exit(404)

    log.debug("Using \"{}\" as csv dir".format(csv_dir))

    config_datasets_ids = []
    if args.ids:
        for elem in args.ids:
            if elem in sections:
                log.debug("Adding \"{}\" as a valid dataset id")
                config_datasets_ids = args.ids
            else:
                log.warning("Dataset {} doesn't exists in config file, not adding it".format(elem))
    else:
        log.info("Taking all datasets in the config file")
        config_datasets_ids = sections

    ch_csv_file_path_list = []
    datasets_ids = []
    for d_id in config_datasets_ids:
        ch_csv_path = csv_dir + '/' + d_id + '.characteristics.csv'
        if not os.path.exists(ch_csv_path):
            log.warning("Path to file \"{}\" doesn't exists. Not adding it.".format(ch_csv_path))
            continue

        log.debug("Adding \"{}\" to CSV path list".format(ch_csv_path))

        datasets_ids.append(d_id)
        ch_csv_file_path_list.append(ch_csv_path)

    csv_to_combination_hashes(datasets_ids, ch_csv_file_path_list, feat_list_name, feature_file_path, output_dir)

    quit()
