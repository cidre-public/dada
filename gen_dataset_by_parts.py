import logging
import math
import os

from count_instances import combinations_hashes_to_classcount
import utilities as ut

import time
now_str = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime())


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def gen_dataset_by_parts(dataset_a, dataset_b, mode, percent=5, fixed_size=0):

    dataset_combinations = [dataset_a, dataset_b]

    dataset_sizes = []
    for elem in dataset_combinations:
        size = ut.get_size_from_combinations(elem)
        dataset_sizes.append(size)
        # log.debug("Size of {}: {}".format(id, size))

    smallest_dataset_index = dataset_sizes.index(min(dataset_sizes))
    biggest_dataset_index = dataset_sizes.index(max(dataset_sizes))

    sample_size = 1
    dataset_to_sample = {}
    dataset_to_merge_with = {}

    method = ''
    m = None
    if mode == 'f':
        log.info("- Mix composed of 50% from smallest and 50% from biggest")
        method = '50/50'
        dataset_to_sample = dataset_combinations[biggest_dataset_index]
        if fixed_size:
            sample_size = fixed_size/2
            if sample_size > dataset_sizes[smallest_dataset_index]:
                log.error("Cannot perform task, sample size = {} while smallest dataset's size is {}"
                          .format(sample_size, dataset_sizes[smallest_dataset_index]))
                exit()
            dataset_to_merge_with = ut.random_from_combinations(dataset_to_merge_with, sample_size)
        else:
            sample_size = dataset_sizes[smallest_dataset_index]
            dataset_to_merge_with = dataset_combinations[smallest_dataset_index]
    elif mode == 'ff':
        log.info("- Mix composed of 50% from first and 50% from second (as much as possible)")
        if fixed_size:
            sample_size = sample_size/2
            if sample_size > dataset_sizes[0]:
                log.error("Cannot perform task, sample size = {} while the first dataset's size is {}"
                          .format(sample_size, dataset_sizes[0]))
                exit()
            dataset_to_merge_with = ut.random_from_combinations(dataset_to_merge_with, sample_size)
            dataset_to_sample = dataset_combinations[biggest_dataset_index]
        else:
            if dataset_sizes[0] <= dataset_sizes[1]:
                sample_size = dataset_sizes[0]
                dataset_to_sample = dataset_combinations[1]
                dataset_to_merge_with = dataset_combinations[0]
            else:
                log.warning("Second dataset not as big as first, taking the most of first dataset")
                sample_size = dataset_sizes[1]
                dataset_to_sample = dataset_combinations[0]
                dataset_to_merge_with = dataset_combinations[1]

        method = '50/50 (first dataset is half the size of the mix)'
    elif mode == 't':
        log.info("1/3, 2/3 mix, taking smallest as reference")
        biggest_dataset_size = dataset_sizes[biggest_dataset_index]
        smallest_dataset_size = dataset_sizes[smallest_dataset_index]

        dataset_to_sample = dataset_combinations[biggest_dataset_index]
        dataset_to_merge_with = dataset_combinations[smallest_dataset_index]

        if biggest_dataset_size >= 2 * smallest_dataset_size:
            log.debug("Normal random")
            sample_size = 2 * smallest_dataset_size
        else:
            log.debug("Alternative random")
            sample_size = math.floor(biggest_dataset_size / 2)
        # method = '1/3 of {}, 2/3 of {}'.format(datasets_id[smallest_dataset_index], datasets_id[
        # biggest_dataset_index])
    elif mode == 'tf':
        log.info("Mix composed of 1/3 from first and 2/3 from second")
        dataset_to_sample = dataset_combinations[1]
        dataset_to_merge_with = dataset_combinations[0]

        if dataset_sizes[1] >= 2 * dataset_sizes[0]:
            sample_size = 2 * dataset_sizes[0]
        else:  # |D2| < 2 * |D1|
            sample_size = math.floor(dataset_sizes[1] / 2)
            dataset_to_sample = dataset_combinations[0]
            dataset_to_merge_with = ut.random_from_combinations(dataset_combinations[1], sample_size)
    elif mode == 'p':
        log.info("Mix with {}% from first dataset".format(percent))
        dataset_to_sample = dataset_combinations[1]
        dataset_to_merge_with = dataset_combinations[0]

        if fixed_size:
            sample_size = fixed_size * percent / 100

        m = (100 - percent)/percent
        log.debug("Multiplier: {}".format(m))
        if dataset_sizes[1] >= m * dataset_sizes[0]:
            log.debug("Case where |D2| >= m * |D1|")
            sample_size = math.floor(m * dataset_sizes[0])
        else:  # |D2| < m * |D1|
            log.debug("Case where |D2| < m * |D1|")
            sample_size = int(math.floor(dataset_sizes[1] / m))
            dataset_to_sample = dataset_combinations[0]
            dataset_to_merge_with = dataset_combinations[1]
    else:
        log.error("No mode selected, exiting")
        exit(33)

    if fixed_size:
        log.debug("Using fixed size here")
        dataset_random_first = ut.random_from_combinations(dataset_to_merge_with, fixed_size)
        dataset_random_second = ut.random_from_combinations(dataset_to_sample, fixed_size)
        mix_dataset = ut.merge_datasets([dataset_random_first, dataset_random_second])
    else:
        dataset_random_big = ut.random_from_combinations(dataset_to_sample, sample_size)
        log.debug("  - Size of biggest dataset: {}".format(ut.get_size_from_combinations(dataset_random_big)))
        log.debug("  - Size of smallest dataset: {}".format(sample_size))
        mix_dataset = ut.merge_datasets([dataset_to_merge_with, dataset_random_big])

    return mix_dataset


def wrapper_gen_dataset_by_parts(dataset_a, dataset_b, dataset_id_list, mode,
                                 output_dir, ds_id, name, feat_list_name, percent=5, fixed_size=None, output_files=True):

    log.debugv("Executing gen_dataset_by_parts")
    # Generated dataset directory
    output_dir = output_dir + '/' + ds_id

    # Create output directory (same name as the id)
    if output_files:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

    dataset_combinations = [ut.load_json_file(dataset_a), ut.load_json_file(dataset_b)]

    dataset_sizes = []
    for elem, id in zip(dataset_combinations, dataset_id_list):
        size = ut.get_size_from_combinations(elem)
        dataset_sizes.append(size)
        log.debug("Size of {}: {}".format(id, size))

    smallest_dataset_index = dataset_sizes.index(min(dataset_sizes))
    biggest_dataset_index = dataset_sizes.index(max(dataset_sizes))

    sample_size = 1
    dataset_to_sample = {}
    dataset_to_merge_with = {}

    method = ''
    m = None
    if mode == 'f':
        log.info("Mix composed of 50% from smallest and 50% from biggest")
        method = '50/50'
        dataset_to_sample = dataset_combinations[biggest_dataset_index]
        if fixed_size:
            sample_size = fixed_size/2
            if sample_size > dataset_sizes[smallest_dataset_index]:
                log.error("Cannot perform task, sample size = {} while smallest dataset's size is {}"
                          .format(sample_size, dataset_sizes[smallest_dataset_index]))
            dataset_to_merge_with = ut.random_from_combinations(dataset_to_merge_with, sample_size)
        else:
            sample_size = dataset_sizes[smallest_dataset_index]
            dataset_to_merge_with = dataset_combinations[smallest_dataset_index]
    elif mode == 'ff':
        log.info("Mix composed of 50% from first and 50% from second (as much as possible)")
        if fixed_size:
            sample_size = sample_size/2
            if sample_size > dataset_sizes[0]:
                log.error("Cannot perform task, sample size = {} while the first dataset's size is {}"
                          .format(sample_size, dataset_sizes[0]))
            dataset_to_merge_with = ut.random_from_combinations(dataset_to_merge_with, sample_size)
            dataset_to_sample = dataset_combinations[biggest_dataset_index]
        else:
            if dataset_sizes[0] <= dataset_sizes[1]:
                sample_size = dataset_sizes[0]
                dataset_to_sample = dataset_combinations[1]
                dataset_to_merge_with = dataset_combinations[0]
            else:
                log.warning("Second dataset not as big as first, taking the most of first dataset")
                sample_size = dataset_sizes[1]
                dataset_to_sample = dataset_combinations[0]
                dataset_to_merge_with = dataset_combinations[1]

        method = '50/50 (first dataset is half the size of the mix)'
    elif mode == 't':
        log.info("1/3, 2/3 mix, taking smallest as reference")
        biggest_dataset_size = dataset_sizes[biggest_dataset_index]
        smallest_dataset_size = dataset_sizes[smallest_dataset_index]

        dataset_to_sample = dataset_combinations[biggest_dataset_index]
        dataset_to_merge_with = dataset_combinations[smallest_dataset_index]

        if biggest_dataset_size >= 2 * smallest_dataset_size:
            log.debug("Normal random")
            sample_size = 2 * smallest_dataset_size
        else:
            log.debug("Alternative random")
            sample_size = math.floor(biggest_dataset_size / 2)
        method = '1/3 of {}, 2/3 of {}'.format(dataset_id_list[smallest_dataset_index], dataset_id_list[biggest_dataset_index])
    elif mode == 'tf':
        log.info("Mix composed of 1/3 from first and 2/3 from second")
        dataset_to_sample = dataset_combinations[1]
        dataset_to_merge_with = dataset_combinations[0]

        if dataset_sizes[1] >= 2 * dataset_sizes[0]:
            sample_size = 2 * dataset_sizes[0]
        else:  # |D2| < 2 * |D1|
            sample_size = math.floor(dataset_sizes[1] / 2)
            dataset_to_sample = dataset_combinations[0]
            dataset_to_merge_with = ut.random_from_combinations(dataset_combinations[1], sample_size)
    elif mode == 'p':
        log.info("Mix with {}% from first dataset".format(percent))
        dataset_to_sample = dataset_combinations[1]
        dataset_to_merge_with = dataset_combinations[0]

        if fixed_size:
            sample_size = fixed_size * percent / 100

        m = (100 - percent)/percent
        log.debug("Multiplier: {}".format(m))
        if dataset_sizes[1] >= m * dataset_sizes[0]:
            log.debug("Case where |D2| >= m * |D1|")
            sample_size = math.floor(m * dataset_sizes[0])
        else:  # |D2| < m * |D1|
            log.debug("Case where |D2| < m * |D1|")
            sample_size = int(math.floor(dataset_sizes[1] / m))
            dataset_to_sample = dataset_combinations[0]
            dataset_to_merge_with = dataset_combinations[1]
    else:
        log.error("No mode selected, exiting")
        exit(33)

    if fixed_size:
        log.debug("Using fixed size here")
        dataset_random_first = ut.random_from_combinations(dataset_to_merge_with, fixed_size)
        dataset_random_second = ut.random_from_combinations(dataset_to_sample, fixed_size)
        mix_dataset = ut.merge_datasets([dataset_random_first, dataset_random_second])
    else:
        dataset_random_big = ut.random_from_combinations(dataset_to_sample, sample_size)
        mix_dataset = ut.merge_datasets([dataset_to_merge_with, dataset_random_big])

    if output_files:
        # *.dataset_class_info.json structure
        output_json = {
            'name': name,
            'size': ut.get_size_from_combinations(mix_dataset),
            'base datasets': dataset_id_list,
            'method': method,
            'sample size': sample_size,
            'multiplier': m
        }
        # Output combinations hashes
        if ds_id:
            output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.combination_hashes.json'
        else:
            output_file = output_dir + '/generated_dataset_algo_cool' + now_str + '.combination_hashes.json'
        ut.dump_json_to_file(mix_dataset, output_file)
        log.info("Generated dataset combinations_hashes saved as {}".format(output_file))

        # Output *.classcount.csv file
        output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.classcount.csv'
        combinations_hashes_to_classcount(mix_dataset, output_dir, ds_id,
                                          output_file, feat_file_name=feat_list_name)
        log.info("Generated dataset classcount saved as {}".format(output_file))

        # Output dataset_class_info file
        if ds_id:
            output_file = output_dir + '/' + ds_id + '.' + feat_list_name + '.dataset_class_info.json'
        else:
            output_file = output_dir + '/generated_dataset_algo_search_plus_' + now_str + '.dataset_class_info.json'

        ut.dump_json_to_file(output_json, output_file, is_numpy=True)
        log.info("Dataset info saved as {}".format(output_dir + '/' + ds_id + '_' + '.dataset_class_info.json'))
        log.info("You may want to include this in your config file:\n"
                 "[{}]\ndataset_name={}\ncamembert_name={}".format(ds_id, name, "${dataset_name}"))

    return mix_dataset

