import subprocess

xps = [
  ("NAZE_deb", "NAZE-18-G", "AZ20_30k", "AZ19_100k"),
  ("Drebin_deb", "Drebin", "AZ20_30k", "AMD VirusShare_2015 VirusShare_2016 VirusShare_2017 VirusShare_2018"),
  ("VS15-18_deb", "VirusShare_2015 VirusShare_2016 VirusShare_2017 VirusShare_2018", "AZ20_30k", "Drebin AMD"),
  ("VS15_deb", "VirusShare_2015", "AZ19_100k_2015", "Drebin"),
  ("VS16_deb", "VirusShare_2016", "AZ19_100k_2016", "Drebin VirusShare_2015"),
  ("VS17_deb", "VirusShare_2017", "AZ19_100k_2017", "Drebin VirusShare_2015 VirusShare_2016 AMD"),
  ("VS18_deb", "VirusShare_2018", "AZ19_100k_2018", "Drebin VirusShare_2015 VirusShare_2016 AMD VirusShare_2017")
]

BEGIN = "python3 debias_script.py config.datasets.ini"
for xp in xps:

  delta = 0.04
  while delta >= 0.00125:
    print("======================================================")
    print(xp) 
    COMMAND = BEGIN
    name = xp[0]
    base_datasets = xp[1]
    if len(base_datasets.split(' ')) > 1:
      base_datasets = '-'.join(base_datasets.split(' '))
    # COMMAND += " " + base_datasets + "-to-" + xp[1] + "-" + str(delta).replace('.', '')
    # COMMAND += " " + base_datasets + "___" + xp[1] + "-" + str(delta).replace('.', '')
    delta_string = str(delta).replace('.', '')[1:]

    COMMAND += " " + name + "-" + delta_string
    COMMAND += " " + name + "-" + delta_string
    COMMAND += " --base-datasets " + xp[1]
    COMMAND += " --target-dataset " + xp[2]
    COMMAND += " --source-datasets " + xp[3]
    COMMAND += " --delta " + str(delta)
    print(COMMAND)
    subprocess.run(COMMAND.split(' '))
    delta = delta / 2
