# Merge csvs
import argparse
import logging
import pandas
import os


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning(
            'Logging level "{}" not defined, setting "normal" instead'.format(level)
        )


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style["debugv"] = {"color": "magenta"}
        coloredlogs.install(
            show_hostname=False,
            show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt="%(asctime)s [%(levelname)8s] %(message)s"
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def merge_these_csvs(csv1_path, csv2_path, output_filepath):
    for file in [csv1_path, csv2_path]:
        if not os.path.exists(file):
            log.error("File {} doesn't exists, exiting".format(file))
            exit(-1)

    if os.path.isdir(output_filename):
        log.error("Path {} is a dir, specify a file. Exiting.".format(output_filename))
        exit(-1)

    output_dir = os.path.dirname(output_filename)
    if not os.path.exists(output_dir):
        log.info("Directory {} does not exists, creating it".format(output_dir))
        os.makedirs(output_dir)

    # data1
    data1 = pandas.read_csv(csv1_path, low_memory=False)
    data2 = pandas.read_csv(csv2_path, low_memory=False).drop(["label"], axis=1)

    # using merge function by setting how='outer'
    output3 = pandas.merge(data1, data2, on="sha256", how="left").fillna(0)

    # displaying result
    # print(output4)

    output3.to_csv(output_filepath, index=False)
    log.info("File saved as {}".format(output_filepath))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge two CSV files")
    parser.add_argument("input_filename1", help="Full path of the first CSV file")
    parser.add_argument("input_filename2", help="Full path of the second CSV file")
    parser.add_argument(
        "output_filename", help="Full path of the output merged CSV file"
    )
    parser.add_argument(
        "-v",
        help="Output information to the standard output (-vv is very verbose)",
        action="count",
    )

    args = parser.parse_args()

    if args.v is not None:
        verbosity = args.v
        print("Verbosity: " + str(verbosity))
    else:
        verbosity = 0

    logSetup(verbosity)
    # log.debug("This is a debug print")
    # log.debugv("This is a very verbose debug print")
    # call my function

    input_filename1 = args.input_filename1
    input_filename2 = args.input_filename2
    output_filename = args.output_filename

    merge_these_csvs(input_filename1, input_filename2, output_filename)
    quit()
