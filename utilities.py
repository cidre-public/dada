import json
import os
import logging
import random
import pandas

import numpy as np


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # # Default format:
            # # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            # #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


def dump_json_to_file(json_data, file_path, is_numpy=False):
    with open(file_path, 'w') as out:
        if is_numpy:
            json.dump(json_data, out, cls=NpEncoder)
        else:
            json.dump(json_data, out)


def load_json_file(json_file_path):
    with open(json_file_path) as f:
        data = json.load(f)
    return data


def hash_lists_to_dict(list_dataset_paths, hash_dict):
    for dataset_path in list_dataset_paths:
        if os.path.exists(dataset_path):
            name = dataset_path.split("/")[-1].split('.')[0]
            with open(dataset_path) as f:
                for line in f:
                    hash_dict[line] = name
        else:
            log.warning("{} doesn't exists, continuing".format(dataset_path))


def load_value_list(file_path):
    this_list = []
    if os.path.exists(file_path):
        with open(file_path) as f:
            for line in f:
                this_list.append(line.strip())
    else:
        log.error("Path {} doesn't exists, return empty list".format(file_path))
    return this_list


def combination_hashes_to_hash_value_dict(combination_hash):
    hash_value_dict = {}
    for combination in combination_hash:
        for element in combination_hash[combination]:
            hash_value_dict[element] = combination

    return hash_value_dict


def combination_hashes_to_hash_value_dict_del(combination_hash):
    hash_value_dict = {}
    list_keys = combination_hash.keys()
    for combination in list_keys:
        for element in combination_hash[combination]:
            hash_value_dict[element] = combination
        del combination_hash[combination]

    return hash_value_dict


def hash_value_dict_to_combination_hashes(hash_value_dict):
    combination_hashes = {}
    list_keys = hash_value_dict.keys()
    for key in list_keys:
        combination = hash_value_dict[key]
        if combination not in combination_hashes:
            combination_hashes[combination] = []
        combination_hashes[combination].append(key)

    return combination_hashes


def random_from_combinations(combination_hashes, size):
    # CH --> HV
    hash_value = combination_hashes_to_hash_value_dict(combination_hashes)

    # Random HV
    new_hash_value = {}
    random_keys = random.sample(list(hash_value.keys()), size)

    for key in random_keys:
        new_hash_value[key] = hash_value[key]

    # HV --> CH
    new_combination_hashes = hash_value_dict_to_combination_hashes(new_hash_value)

    return new_combination_hashes


def merge_datasets(list_combinations_hashes):
    merged_dataset = {}
    for dataset in list_combinations_hashes:
        for combination in dataset:
            if combination not in merged_dataset:
                merged_dataset[combination] = []
            for h in dataset[combination]:
                if h not in merged_dataset[combination]:
                    merged_dataset[combination].append(h)

    return merged_dataset


def difference_combination_hashes(dataset_a, dataset_b):
    diff_dataset = {}
    list_classes_a = dataset_a.keys()
    for c in list_classes_a:
        if c not in dataset_b:
            continue
        diff_elems_of_class = [e for e in dataset_a[c] if e not in dataset_b[c]]

        if diff_elems_of_class:
            diff_dataset[c] = diff_elems_of_class

    return diff_dataset


def generate_matrix_file(dataset, sources_matrix_path_list, output_dir, id):
    # Get first row of any source_dataset matrix file
    # File existance checked beforehand
    with open(sources_matrix_path_list[0]) as f:
        header = f.readline().strip()

    # Get all hashes of the dataset
    dataset_hash_list = []
    for combo in dataset:
        dataset_hash_list.extend(dataset[combo])

    log.debug("dataset hash list size: {}".format(len(dataset_hash_list)))
    log.debug("Using the following paths of sources: {}".format(sources_matrix_path_list))

    # For each matrix file
    matrix_lines = []
    already_seen = []
    for source_matrix_path in sources_matrix_path_list:
        with open(source_matrix_path) as f:
            f.readline()

            # For each hash in the matrix file (pass first line)
            for linen in f.readlines():
                line = linen.strip()
                this_hash = line.split(',')[0]

                # If hash in the list of hashes: put the information in output list
                if this_hash in dataset_hash_list and this_hash not in already_seen:
                    matrix_lines.append(line)
                    already_seen.append(this_hash)

    log.debug("Matrix file has {} lines".format(len(matrix_lines)))
    # Save output list
    filename = output_dir + '/' + id + '/' + id + '.matrix.csv'
    with open(filename, 'w') as f:
        f.write(header + "\n")
        for line in matrix_lines:
            f.write(line + "\n")

    log.info("File saved as {}".format(filename))


def generate_characteristics_file(dataset, sources_char_path_list, output_dir, id):
    # Get first row of any source_dataset matrix file
    # File existance checked beforehand
    with open(sources_char_path_list[0]) as f:
        header = f.readline().strip()

    # Get all hashes of the dataset
    dataset_hash_list = []
    for combo in dataset:
        dataset_hash_list.extend(dataset[combo])

    log.debug("dataset hash list size: {}".format(len(dataset_hash_list)))
    log.debug("Using the following paths of sources: {}".format(sources_char_path_list))

    # For each matrix file
    char_lines = []
    already_seen = []
    for source_characteristics_path in sources_char_path_list:
        with open(source_characteristics_path) as f:
            f.readline()

            # For each hash in the matrix file (pass first line)
            for linen in f.readlines():
                line = linen.strip()
                this_hash = line.split(',')[0]

                # If hash in the list of hashes: put the information in output list
                if this_hash in dataset_hash_list and this_hash not in already_seen:
                    char_lines.append(line)
                    already_seen.append(this_hash)

    log.debug("Characteristics file has {} lines".format(len(char_lines)))

    # Save sha256 file
    filename = output_dir + '/' + id + '/' + id + '.sha256.txt'
    with open(filename, 'w') as f:
        for line in already_seen:
            f.write(line + "\n")

    log.info("File saved as {}".format(filename))

    # Save output list
    filename = output_dir + '/' + id + '/' + id + '.characteristics.csv'
    with open(filename, 'w') as f:
        f.write(header + "\n")
        for line in char_lines:
            f.write(line + "\n")

    log.info("File saved as {}".format(filename))


def get_size_from_combinations(dataset_classes):
    size = 0
    for k in dataset_classes:
        size += len(dataset_classes[k])

    return size


class Dataset:
    def __init__(self, combination_hashes, class_size_ext=None):
        self.items = combination_hashes
        if class_size_ext:
            self.class_size = class_size_ext
        else:
            s = {}
            for k in combination_hashes:
                s[k] = len(combination_hashes[k])
            self.class_size = s

        # self.s = PseudoPandas(s)

        self.total = 0
        self.index = list(self.class_size.keys())
        for k in self.class_size:
            self.total += self.class_size[k]

    def get(self, k):
        return self.class_size[k]

    def sum(self):
        return self.total

    def update(self, k, num):
        self.class_size[k] += num
        self.total += num

    def get_non_empty(self):
        list_k = []
        for k in self.class_size:
            if self.class_size[k] > 0:
                list_k.append(k)
        return list_k

    def remove_one_app(self, k):
        this_hash = random.choice(self.items[k])
        self.items[k].remove(this_hash)

        # num_class = self.s.get(k)
        # self.s.update(pandas.Series([num_class - 1], index=[k]))
        # self.update(k, -1)
        return this_hash, k

    def add_one_app(self, this_hash, k):
        self.items[k].append(this_hash)

        # num_class = self.s.get(k)
        # self.s.update(pandas.Series([num_class + 1], index=[k]))
        # self.update(k, 1)
