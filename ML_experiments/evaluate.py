import os
import pandas as pd
import sklearn.metrics
import sys
from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier

for file in [
    sys.argv[1] + "_tr_x.csv",
    sys.argv[1] + "_tr_y.csv",
    sys.argv[2] + "_te_x.csv",
    sys.argv[2] + "_te_y.csv",
]:
    if not os.path.exists(file):
        print("[Error] File {} does not exists, exiting".format(file))
        exit(-1)

train_x = pd.read_csv(sys.argv[1] + "_tr_x.csv", low_memory=False)
train_y = pd.read_csv(sys.argv[1] + "_tr_y.csv", low_memory=False)
test_x = pd.read_csv(sys.argv[2] + "_te_x.csv", low_memory=False)
test_y = pd.read_csv(sys.argv[2] + "_te_y.csv", low_memory=False)
print(
    "\n  Learning with",
    sys.argv[1],
    "(" + str(len(train_x)) + ")",
    "and test with",
    sys.argv[2],
    "(" + str(len(test_x)) + ")",
)

intersection = pd.merge(train_x, test_x, how="inner", on=["sha256"]).drop_duplicates(
    subset="sha256"
)

# Since we need to remove the intersection from the training set, it is trival to optimize the experiments by learning once and testing with various test sets.

if len(intersection) > 0:
    print("Removed intersection (size: " + str(len(intersection)) + ") from train set")
    train_x = pd.DataFrame(
        pd.concat([train_x, test_x, test_x]).drop_duplicates(
            subset="sha256", keep=False
        )
    )
    train_y = pd.DataFrame(
        pd.concat([train_y, test_y, test_y]).drop_duplicates(
            subset="sha256", keep=False
        )
    )

train_x.drop(columns=["sha256"], inplace=True)
test_x.drop(columns=["sha256"], inplace=True)
train_y.drop(columns=["sha256"], inplace=True)
train_y = train_y.to_numpy().ravel()
test_y.drop(columns=["sha256"], inplace=True)

classifiers = [
    [KNeighborsClassifier()],
    [tree.DecisionTreeClassifier(random_state=i) for i in range(25)],
    [RandomForestClassifier(random_state=i) for i in range(25)],
    [GaussianNB()],
    [AdaBoostClassifier(random_state=i) for i in range(25)],
]

print("Starting the classifiers")

mean_auc = 0
max_auc = 0
for c in classifiers:
    tmp_auc = 0
    for classifier in c:
        classifier = classifier.fit(train_x, train_y)
        y_pred = classifier.predict(test_x)
        tmp_auc += sklearn.metrics.roc_auc_score(test_y, y_pred)
    auc = tmp_auc / len(c)
    print(type(c[0]).__name__, "(x" + str(len(c)) + ")", "AUC:", auc)
    mean_auc += auc
    if auc > max_auc:
        max_auc = auc
mean_auc = mean_auc / len(classifiers)
print("  Mean AUC: ", mean_auc)
print("  Max AUC: ", max_auc)
