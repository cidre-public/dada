#!/bin/bash

trap "exit" INT
mkdir -p datasets

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train DR-AG-training -o datasets/drag14$1

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train VS-AG-training -test VS-AG-test -o datasets/vsag17$1

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train VS-AG_deb-training -test VS-AG_deb-test -o datasets/vsagdeb17$1

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train AZL17-training -test AZL17-test -o datasets/azl17$1

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train ACT17-training -test ACT17-test -o datasets/act17$1

echo -e "\n\t-----------\n"
for train in datasets/act17$1 datasets/azl17$1 datasets/vsag17$1 datasets/vsagdeb17$1 datasets/drag14$1; do
    for testing in datasets/act17$1 datasets/azl17$1 datasets/vsag17$1 datasets/vsagdeb17$1; do
        python3 evaluate.py $train $testing
    done
    echo -e "\n\t-----------\n"
done
