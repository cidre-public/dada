#!/bin/bash

trap "exit" INT
mkdir -p datasets

# build the dataset
python3 build_datasets.py -dir ../datasets -droidlysis $1 -train DR-AG-training -o datasets/drag14$1

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train DR-AG_deb-training -test DR-AG_deb-test -o datasets/dragdeb14$1

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train AZL14-training -test AZL14-test -o datasets/azl14$1

python3 build_datasets.py -dir ../datasets -droidlysis $1 -train ACT14-training -test ACT14-test -o datasets/act14$1

for train in datasets/act14$1 datasets/azl14$1 datasets/drag14$1 datasets/dragdeb14$1; do
    for testing in datasets/act14$1 datasets/azl14$1 datasets/dragdeb14$1; do
        python3 evaluate.py $train $testing
    done
    echo -e "\n\t-----------\n"
done
