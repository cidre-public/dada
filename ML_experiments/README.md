# Abstract

This experiment aims at evaluating debiased and non-debiased dataset with a detection task, based on machine learning algorithms.

# Setup

To run this experiment, you need Python 3 and PyPI. You can install the dependency by running :

    pip install -r requirements.txt

To redo our ML experiments with the FalDroid features, we give the `arff` files and the `*.merged_characteristics` files for the datasets in Tables IV and V of the article. Optionally, these last files can be regenerated using the [merge script](../merge_characteristics_faldroid.sh):

```bash
bash merge_characteristics_faldroid.sh
```

This will merge the characteristics from previous steps with the features from FalDroid.

# How to start the experiments (on a Unix system)

To run the experiments with pivot year 2014 (Table V in the article), run:

    ./run_expe_14.sh -faldroid

To run the experiments with pivot year 2017 (Table IV in the article), run:

    ./run_expe_17.sh -faldroid

To run the experiments with pivot year 2014 without the Faldroid features, run:

    ./run_expe_14.sh

To run the experiments with pivot year 2017 without the Faldroid features, run:

    ./run_expe_17.sh

The experiments may take an hour to complete.

# Experiments output

The output looks like this example:

``` Learn with azl14 (4889) and test with dragdeb14 (40)
KNeighborsClassifier (x1) AUC: 0.763888888888889
DecisionTreeClassifier (x25) AUC: 0.603333333333333
RandomForestClassifier (x25) AUC: 0.6927777777777777
GaussianNB (x1) AUC: 0.986111111111111
AdaBoostClassifier (x25) AUC: 0.6388888888888892
  Mean AUC:  0.737
  Max AUC:  0.986111111111111
```

The first line contains the training set (AZL 2014 in this example) and the test set (DR-AG_Deb 2014 in this example). Remark: there may be a slight discrepancy between the dataset size in the `datasets` folder and the number presented here. Indeed, when Droidlysis can't extract features, it will replace them with the "unknown" value. These few instances are removed for this experiment as some algorithms may fail to process it.

The line `Removed intersection (size: 2) from train set` may appear sometimes. It means that there were some examples both in training and test sets and were removed from the training set.

The five following lines detail the performances of various classifiers. On the left, there is the classifier's name (more precisely, the class name in sklearn), the number of executions, and its mean area under the ROC curve (AUC).

Finally, the mean and may AUC are displayed on the last line.
