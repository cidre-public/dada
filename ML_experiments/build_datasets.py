import pandas as pd
import sys
import os
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier


def build(dataset, dataset_class):
    dataset = pd.merge(dataset, dataset_class, on="sha256")
    dataset = dataset.drop_duplicates(subset="sha256")
    # remove some superfluous features
    dataset.drop(
        columns=["apk_size", "dex_date", "dataset", "Year", "year", "APK size"],
        inplace=True,
    )
    # Remove lines with "unknown" values
    for k in dataset.columns.values:
        dataset = dataset.loc[dataset[k] != "unknown"]
    dataset_x = dataset.drop(columns=["malware"])
    dataset_y = dataset[["sha256", "malware"]]
    return dataset_x, dataset_y


train = None
test = None
use_droidlysis = False
use_faldroid = False
carac_path = ""
all_class = []
i = 1
base_path = ""

while i < len(sys.argv):
    if sys.argv[i] == "-dir":  # must appear before -train and -test
        i += 1
        base_path = sys.argv[i]

    elif sys.argv[i] == "-faldroid":
        use_faldroid = True
        if use_droidlysis:
            carac_path = "merged_characteristics"
        else:
            carac_path = "graph_characteristics"

    elif sys.argv[i] == "-droidlysis":
        use_droidlysis = True
        if use_faldroid:
            carac_path = "merged_characteristics"
        else:
            carac_path = "characteristics"

    elif sys.argv[i] == "-train":
        i += 1
        train_path = os.path.join(base_path, sys.argv[i])
        if not os.path.exists(train_path + "." + carac_path + ".csv"):
            print(
                "[Error] Train file {} does not exists, exiting".format(
                    train_path + "." + carac_path + ".csv"
                )
            )
            exit(-1)
        train = pd.read_csv(train_path + "." + carac_path + ".csv", low_memory=False)
        all_class.append(pd.read_csv(train_path + ".goodmal.csv", low_memory=False))

    elif sys.argv[i] == "-test":
        i += 1
        test_path = os.path.join(base_path, sys.argv[i])
        if not os.path.exists(test_path + "." + carac_path + ".csv"):
            print(
                "[Error] Train file {} does not exists, exiting".format(
                    test_path + "." + carac_path + ".csv"
                )
            )
            exit(-1)
        test = pd.read_csv(test_path + "." + carac_path + ".csv", low_memory=False)
        all_class.append(pd.read_csv(test_path + ".goodmal.csv", low_memory=False))

    elif sys.argv[i] == "-o":
        i += 1
        train_output = sys.argv[i] + "_tr"
        test_output = sys.argv[i] + "_te"

    else:
        print("Error:", sys.argv[i])
        exit()
    i += 1

if train_output == None:
    print("No output files !")
    exit()

all_class = pd.concat(all_class)
if train is not None:
    train_x, train_y = build(train, all_class)
    train_x.to_csv(train_output + "_x.csv", index=False)
    print("File saved as {}".format(train_output + "_x.csv"))
    train_y.to_csv(train_output + "_y.csv", index=False)
    print("File saved as {}".format(train_output + "_y.csv"))


if test is not None:
    test_x, test_y = build(test, all_class)
    test_x.to_csv(test_output + "_x.csv", index=False)
    print("File saved as {}".format(test_output + "_x.csv"))
    test_y.to_csv(test_output + "_y.csv", index=False)
    print("File saved as {}".format(test_output + "_y.csv"))

if train is not None and test is not None:
    intersection = pd.merge(train, test, how="inner", on=["sha256"]).drop_duplicates(
        subset="sha256"
    )
    if len(intersection) > 0:
        print("Intersection size: ", len(intersection))
