import logging
import os
from chi_squared import get_counts
import pandas as pd
import count_instances as ci


def debugv(self, message, *args, **kws):
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, message, args, **kws)


# Adds a very verbose level of logs
DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")
logging.Logger.debugv = debugv
log = logging.getLogger("featslist")


def logSetup(level):
    if level == 2:
        log.setLevel(DEBUG_LEVELV_NUM)
        log.info("Debug is Very Verbose.")
    elif level == 1:
        log.setLevel(logging.DEBUG)
        log.info("Debug is Verbose.")
    elif level == 0:
        log.setLevel(logging.INFO)
        log.info("Debug is Normal.")
    else:
        log.setLevel(logging.INFO)
        log.warning("Logging level \"{}\" not defined, setting \"normal\" instead"
                    .format(level))


# Tries to apply colors to logs
def applyColorsToLogs():
    try:
        import coloredlogs

        style = coloredlogs.DEFAULT_LEVEL_STYLES
        style['debugv'] = {'color': 'magenta'}
        coloredlogs.install(
            show_hostname=False, show_name=True,
            logger=log,
            level=DEBUG_LEVELV_NUM,
            fmt='%(asctime)s [%(levelname)8s] %(message)s'
            # Default format:
            # fmt='%(asctime)s %(hostname)s %(name)s[%(process)d] %(levelname)s
            #  %(message)s'
        )
    except ImportError:
        log.error("Can't import coloredlogs, logs may not appear correctly.")


applyColorsToLogs()


def get_counts_combinations(count_data_path):
    """
    Function for counting combination classes in datasets. It returns:
        - dataset_count_dict: A dictionary of counts, with dataset name as key (dataset_count_dict)
        - combination_list: A list of combinations (in the order as seen by looking linearly the dataset)
        - combination_name_dict: A map of combination tuple to name
        - count_dict_by_combination: A map of combination tuple to the number of times seen in the dataset

    :param count_data_path: List of paths to count files (typically .classcount.csv files)
    :return: dataset_count_dict, combination_list, combination_name_dict, count_dict_by_combination
    """

    # Dictionary of dataset names, with its count (a dictionary) and the total counts
    dataset_count_dict = {}
    # The list of combinations as they appear by reading the counts
    combination_list = []

    # Get the counts from the .classcount.csv files
    for count_data in count_data_path:
        this_count, total_counts = get_counts(count_data)
        count_name = count_data.strip().split("/")[-1].split('.')[0]

        # Put combinations (tuples) in list (if they don't already exists)
        for combination in this_count:
            if combination not in combination_list:
                combination_list.append(combination)
        dataset_count_dict[count_name] = {'count': this_count, 'total_counts': total_counts}

    # Container for the lists of combinations
    # count_lists = []
    count_dict_by_combination = {}

    # Get a list of lists, where the first order are combinations, and for each combination,
    # the percentage according to the list of datasets in 'all_counts'
    for index, combination in enumerate(combination_list):
        # count_list = []
        count_proportion_dict = {}
        # For each dataset
        for dataset in dataset_count_dict:
            log.debugv("In dataset {}, combination {}".format(dataset, combination))
            dataset_count = dataset_count_dict[dataset]['count']
            dataset_total = dataset_count_dict[dataset]['total_counts']

            # Get the combination proportion in a list (zero if there is none)
            if combination in dataset_count:
                # count_list.append(dataset_count[combination]/dataset_total)
                count_proportion_dict[dataset] = dataset_count[combination]/dataset_total
            else:
                count_proportion_dict[dataset] = 0
                # count_list.append(0)
        # Append the list to the container
        # count_lists.append(count_list)
        count_dict_by_combination[index] = count_proportion_dict

    return dataset_count_dict, combination_list, count_dict_by_combination


def get_class_proportion(feature_list_path, count_data_path, output_dir, topnum=10):

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    feature_list = ci.get_unsorted_selected_feats(feature_list_path)  # tuple(sorted(list(set(selected_features))))

    log.debug('The new feature list: ' + str(feature_list)
              + ' of lenght ' + str(len(feature_list)))

    all_counts, combination_list, \
        count_dict_by_combination = get_counts_combinations(count_data_path, feature_list)

    df = pd.DataFrame(count_dict_by_combination)
    log.info("Printing table")
    print(df)
    log.info("Printing transposed table")
    tdf = df.T
    print(tdf)
    for col in tdf:
        log.info("Top {} for {}:".format(str(topnum), col))
        print(tdf.nlargest(topnum, col)[col])

