#!/bin/bash
# Convert arff files to csv, then merge them with the characteristics file

trap "exit" INT

datasets_dir="datasets"

arff_files=$(find "$datasets_dir" -type f -name "*.arff")

while IFS= read -r -u 3 arff_path || [ -n "$arff_path" ]; do

    echo "Using file $arff_path"
    dataset=$(basename "$arff_path" | cut -d. -f1)
    echo "Dataset is $dataset"
    python3 arff_to_csv.py "$arff_path" -o "$datasets_dir"
    python3 merge_csvs.py "$datasets_dir/$dataset.characteristics.csv" \
        "$datasets_dir/$dataset.graph_characteristics.csv" \
        "$datasets_dir/$dataset.merged_characteristics.csv"
    # read -n 1 -s -r -p "Press any key to continue" && echo

done 3< <(printf '%s\n' "$arff_files")
